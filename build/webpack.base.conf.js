const vueLoaderPlugin = require('vue-loader/lib/plugin');
const path = require('path');
const utils = require('./utils.js');
const webpack = require('webpack');

module.exports = {
    resolve: {
        alias: {
            vue$: 'vue/dist/vue.esm.js',
            '@assets': path.resolve(__dirname, '../src/assets'),
            '@components': path.resolve(__dirname, '../src/components'),
            '@pages': path.resolve(__dirname, '../src/pages/'),
            '@config': path.resolve(__dirname, '../src/config/'),
            '@api': path.resolve(__dirname, '../src/api/'),
            '@utils': path.resolve(__dirname, '../src/utils/')
        },
        extensions: ['.ts', '.js', '.vue', '.json']
    },
    devtool:'source-map',
    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.(js|vue)$/,
                use: ['source-map-loader']
            },
            {
                test: /\.vue$/,
                use: ['vue-loader']
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader'
                    }
                ]
            },
            {
                test: /\.(png|jpeg|gif|jpg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8196,
                            name: utils.assetsPath('image/[name].[hash:7].[ext]')
                        }
                    }
                ]
            },
            {
                test: /\.(woff|ttf)(\?[a-z0-9=]+)?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: utils.assetsPath('font/[name].[hash:7].[ext]'),
                            limit: 8196
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new vueLoaderPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        })
    ]
};
