const path = require('path');
const merge = require('webpack-merge')
const config = require('./config.js')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const prod = config.prod;



const prodConfig = merge(require('./webpack.base.conf.js'),
    {
        mode: 'production',
        entry: {
            app: './src/dev-prod.js'
        },
        output: {
            filename:  '[name].[hash:7].js',
            path: path.resolve(__dirname, '../dist')
        },
        module:{
            rules:[
                {
                    test: /\.(scss|css)$/,
                    use: [
                        {
                            loader : MiniCssExtractPlugin.loader ,
                            options: {publicPath: prod.publicPath}
                            },
                        'css-loader',
                        'resolve-url-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                                // sourceMapContents: false api上有 实际没有
                            }
                        }
                    ]
                }
            ]
        },
        performance: {
            maxAssetSize: 4000000,
            maxEntrypointSize: 5000000
        },
        plugins:[
            new MiniCssExtractPlugin({
                filename: '[name].[hash:7].css'
            }),
            new HtmlWebpackPlugin({
                inject: true,
                filename: 'index.html',
                template: path.resolve(__dirname, '../index.html')
            })
        ]
    }
);

module.exports = prodConfig;
