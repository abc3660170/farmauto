module.exports = {
    dev: {
        publicPath:'/',
        assetsPath: 'assets'
    },
    prod: {
        publicPath: './',
        assetsPath: './assets'
    }
}
module.exports.externals = [{
    vue: 'vue'
}];


