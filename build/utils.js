const path = require('path');
const config = require('./config.js');


exports.assetsPath = function (_path) {
    const assetsPath = process.env.NODE_ENV === 'production'
        ? config.prod.assetsPath
        : config.dev.assetsPath;
    return path.posix.join(assetsPath, _path);
};
