const path = require('path');
const webpack = require('webpack');
const baseConfig = require('./webpack.base.conf.js');
const merge = require('webpack-merge');
const utils = require('./utils.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const devConfig = merge(
    baseConfig,
    {
        mode: 'development',
        entry: {
            app: './src/dev-prod.js'
        },
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, '../tmp')
        },
        module:{
            rules:[
                {
                enforce: "pre",
                test: /\.(js|vue)$/,
                use: ['source-map-loader']
            },
                {
                    enforce: "pre",
                    test: /\.(js|vue)$/,
                    exclude: /node_modules/,
                    use: [
                            {
                                loader: 'eslint-loader',
                                options: {
                                    fix: true
                                }
                            }
                    ]
                },
                {
                    test: /\.(scss|css)$/,
                    use: ['style-loader', 'css-loader', 'resolve-url-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                                // sourceMapContents: false api上有 实际没有
                            }
                        }
                    ]
                }
            ]
        },
        devServer: {
            hot: true,
            host:'0.0.0.0',
            useLocalIp: true
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new MiniCssExtractPlugin({
                filename: utils.assetsPath('css/[name].[contenthash].css')
            }),
            new HtmlWebpackPlugin({
                inject: true,
                filename: 'index.html',
                template: path.resolve(__dirname, '../index.html')
            })
        ]
    }
);

module.exports = devConfig;
