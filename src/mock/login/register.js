import { register } from '@api/register';
const { url, mocked, type } = register;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    data: {
        result: true
    },
    msg: 'ok'
};
export default obj;
