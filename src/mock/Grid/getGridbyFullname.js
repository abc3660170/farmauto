import { getGridByFullName } from '@api/grid';
const { url, mocked, type } = getGridByFullName;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    data: {
        id: '2c99c55674e977e40174e97d861a006b',
        createTime: '2020-10-02 21:25:52',
        updateTime: '2020-10-02 21:25:52',
        remark: null,
        createBy: null,
        deleteFlag: false,
        projectId: '2c823f84ef4011eaab5700163e101bca',
        gridCode: 'WG00010012',
        gridName: '水田2号',
        superiorGridid: null,
        gridLevel: 1,
        gridFullname: '水田2号',
        gridLeaderTel: null,
        gridLeaderID: null,
        gridLayoutPicture: null,
        gridcenterX: 119.51502,
        gridcenterY: 32.204979,
        superiorGridname: null,
        gridPathIds: null,
        fidSize: 10317.1,
        fidPlant: '',
        fidPlantName: null,
        fidType: '',
        fidTypeName: null,
        fidAddr: '',
        gridPos: [
            {
                gridId: '2c99c55674e977e40174e97d861a006b',
                gridCode: 'WG00010012',
                posIndex: null,
                posX: 119.51422,
                posY: 32.204579,
                id: '2c99c55674e977e40174e97d8648006c'
            },
            {
                gridId: '2c99c55674e977e40174e97d861a006b',
                gridCode: 'WG00010012',
                posIndex: null,
                posX: 119.51582,
                posY: 32.204579,
                id: '2c99c55674e977e40174e97d865c006d'
            },
            {
                gridId: '2c99c55674e977e40174e97d861a006b',
                gridCode: 'WG00010012',
                posIndex: null,
                posX: 119.51502,
                posY: 32.205779,
                id: '2c99c55674e977e40174e97d866f006e'
            }
        ]
    },
    msg: 'ok'
};
export default obj;
