import { getGridTreeByPlantId } from '@api/grid';
const { url, mocked, type } = getGridTreeByPlantId;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '通过ID或全名获取网格对象成功',
    data: {
        fidPlant: null,
        count: 4,
        children: [
            {
                fidPlant: '001',
                id: '001',
                labelName: '水稻',
                count: 3,
                children: [
                    {
                        id: '2c99c55674e7c2410174e877c6a70961',
                        gridName: '旱田1-1号',
                        labelName: '旱田1-1号',
                        gridCode: 'WG000100090001',
                        gridFullname: '旱田1号->旱田1-1号',
                        superiorGridid: '2c99c55674e7c2410174e876fcef094f',
                        superiorGridname: null,
                        gridLevel: 2,
                        fidSize: 10207.9,
                        fidPlant: '001',
                        fidPlantName: null,
                        fidType: '001',
                        fidTypeName: null,
                        fidAddr: '',
                        projectId: null,
                        gridPos: null,
                        nodeName: '旱田1-1号',
                        nodeID: '2c99c55674e7c2410174e877c6a70961',
                        nodeLevel: 2,
                        nodeParentID: '2c99c55674e7c2410174e876fcef094f',
                        nodeParentName: null
                    },
                    {
                        id: '2c99c55674e8f7ec0174e920892a0225',
                        gridName: '旱田2-1号',
                        labelName: '旱田2-1号',
                        gridCode: 'WG000100100001',
                        gridFullname: '旱田2号->旱田2-1号',
                        superiorGridid: '2c99c55674e7c2410174e87761030958',
                        superiorGridname: null,
                        gridLevel: 2,
                        fidSize: 1212,
                        fidPlant: '001',
                        fidPlantName: null,
                        fidType: '001',
                        fidTypeName: null,
                        fidAddr: '',
                        projectId: null,
                        gridPos: null,
                        nodeName: '旱田2-1号',
                        nodeID: '2c99c55674e8f7ec0174e920892a0225',
                        nodeLevel: 2,
                        nodeParentID: '2c99c55674e7c2410174e87761030958',
                        nodeParentName: null
                    }
                ]
            },
            {
                fidPlant: '002',
                id: '002',
                labelName: '玉米',
                count: 1,
                children: [
                    {
                        id: '2c99c55674e977e40174e97d861a006b',
                        gridName: '水田2号',
                        labelName: '水田2号',
                        gridCode: 'WG00010012',
                        gridFullname: '水田2号',
                        superiorGridid: null,
                        superiorGridname: null,
                        gridLevel: 1,
                        fidSize: 10317.1,
                        fidPlant: '002',
                        fidPlantName: null,
                        fidType: '002',
                        fidTypeName: null,
                        fidAddr: '',
                        projectId: null,
                        gridPos: null,
                        nodeName: '水田2号',
                        nodeID: '2c99c55674e977e40174e97d861a006b',
                        nodeLevel: 1,
                        nodeParentID: null,
                        nodeParentName: null
                    }
                ]
            }
        ]
    }
};
export default obj;
