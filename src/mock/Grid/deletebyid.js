import { deleteById } from '@api/grid';
const { url, mocked, type } = deleteById;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    data: {},
    msg: 'ok'
};
export default obj;
