import { save } from '@api/grid';
const { url, mocked, type } = save;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    data: {
        id: '001'
    },
    msg: 'ok'
};
export default obj;
