import { list } from '@api/grid';
const { url, mocked, type } = list;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '获取所有网格信息成功',
    data: [
        {
            id: '2c99c55674e7c2410174e876fcef094f',
            gridName: '旱田1号',
            gridCode: 'WG00010009',
            gridFullname: '旱田1号',
            superiorGridid: null,
            superiorGridname: null,
            gridLevel: 1,
            fidSize: 10207.9,
            fidPlant: '',
            fidPlantName: null,
            fidType: '',
            fidTypeName: null,
            fidAddr: '',
            projectId: null,
            gridPos: [
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.121212,
                    posY: 29.2121,
                    id: '2c99c55674ebff330174ec32cd170320'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.1321212,
                    posY: 29.2211,
                    id: '2c99c55674ebff330174ec32cd270321'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.122212,
                    posY: 29.2221,
                    id: '2c99c55674ebff330174ec32cd370322'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.121212,
                    posY: 29.2121,
                    id: '2c99c55674ebff330174ec315adb0301'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.1321212,
                    posY: 29.2211,
                    id: '2c99c55674ebff330174ec315aec0302'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.122212,
                    posY: 29.2221,
                    id: '2c99c55674ebff330174ec315b040303'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.121212,
                    posY: 29.2121,
                    id: '2c99c55674ebff330174ec2e0dd502b8'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.1321212,
                    posY: 29.2211,
                    id: '2c99c55674ebff330174ec2e0de602b9'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.122212,
                    posY: 29.2221,
                    id: '2c99c55674ebff330174ec2e0df702ba'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.121212,
                    posY: 29.2121,
                    id: '2c99c55674ebff330174ec2d947e02ab'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.1321212,
                    posY: 29.2211,
                    id: '2c99c55674ebff330174ec2d949202ac'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 139.122212,
                    posY: 29.2221,
                    id: '2c99c55674ebff330174ec2d94a302ad'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 119.519251,
                    posY: 32.203143,
                    id: '2c99c55674e7c2410174e876fcff0950'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 119.520851,
                    posY: 32.203143,
                    id: '2c99c55674e7c2410174e876fd110951'
                },
                {
                    gridId: '2c99c55674e7c2410174e876fcef094f',
                    gridCode: 'WG00010009',
                    posIndex: null,
                    posX: 19.520051,
                    posY: 32.204343,
                    id: '2c99c55674e7c2410174e876fd220952'
                }
            ],
            nodeName: '旱田1号',
            nodeID: '2c99c55674e7c2410174e876fcef094f',
            nodeLevel: 1,
            nodeParentID: null,
            nodeParentName: null
        },
        {
            id: '2c99c55674e7c2410174e877c6a70961',
            gridName: '旱田1-1号',
            gridCode: 'WG000100090001',
            gridFullname: '旱田1号->旱田1-1号',
            superiorGridid: '2c99c55674e7c2410174e876fcef094f',
            superiorGridname: '旱田1号',
            gridLevel: 2,
            fidSize: 10207.9,
            fidPlant: '001',
            fidPlantName: '水稻',
            fidType: '001',
            fidTypeName: '旱田',
            fidAddr: '',
            projectId: null,
            gridPos: [
                {
                    gridId: '2c99c55674e7c2410174e877c6a70961',
                    gridCode: 'WG000100090001',
                    posIndex: null,
                    posX: 119.519251,
                    posY: 32.203143,
                    id: '2c99c55674e7c2410174e877c6b80962'
                },
                {
                    gridId: '2c99c55674e7c2410174e877c6a70961',
                    gridCode: 'WG000100090001',
                    posIndex: null,
                    posX: 119.520851,
                    posY: 32.203143,
                    id: '2c99c55674e7c2410174e877c6c90963'
                },
                {
                    gridId: '2c99c55674e7c2410174e877c6a70961',
                    gridCode: 'WG000100090001',
                    posIndex: null,
                    posX: 19.520051,
                    posY: 32.204343,
                    id: '2c99c55674e7c2410174e877c6d90964'
                }
            ],
            nodeName: '旱田1-1号',
            nodeID: '2c99c55674e7c2410174e877c6a70961',
            nodeLevel: 2,
            nodeParentID: '2c99c55674e7c2410174e876fcef094f',
            nodeParentName: '旱田1号'
        },
        {
            id: '2c99c55674e7c2410174e87761030958',
            gridName: '旱田2号',
            gridCode: 'WG00010010',
            gridFullname: '旱田2号',
            superiorGridid: null,
            superiorGridname: null,
            gridLevel: 1,
            fidSize: 10207.9,
            fidPlant: '',
            fidPlantName: null,
            fidType: '',
            fidTypeName: null,
            fidAddr: '',
            projectId: null,
            gridPos: [
                {
                    gridId: '2c99c55674e7c2410174e87761030958',
                    gridCode: 'WG00010010',
                    posIndex: null,
                    posX: 139.121212,
                    posY: 29.2121,
                    id: '2c99c55674ebff330174ec2efb8302cd'
                },
                {
                    gridId: '2c99c55674e7c2410174e87761030958',
                    gridCode: 'WG00010010',
                    posIndex: null,
                    posX: 139.1321212,
                    posY: 29.2211,
                    id: '2c99c55674ebff330174ec2efb9402ce'
                },
                {
                    gridId: '2c99c55674e7c2410174e87761030958',
                    gridCode: 'WG00010010',
                    posIndex: null,
                    posX: 139.122212,
                    posY: 29.2221,
                    id: '2c99c55674ebff330174ec2efba602cf'
                },
                {
                    gridId: '2c99c55674e7c2410174e87761030958',
                    gridCode: 'WG00010010',
                    posIndex: null,
                    posX: 119.519251,
                    posY: 32.203143,
                    id: '2c99c55674e7c2410174e87761140959'
                },
                {
                    gridId: '2c99c55674e7c2410174e87761030958',
                    gridCode: 'WG00010010',
                    posIndex: null,
                    posX: 119.520851,
                    posY: 32.203143,
                    id: '2c99c55674e7c2410174e8776126095a'
                },
                {
                    gridId: '2c99c55674e7c2410174e87761030958',
                    gridCode: 'WG00010010',
                    posIndex: null,
                    posX: 19.520051,
                    posY: 32.204343,
                    id: '2c99c55674e7c2410174e8776136095b'
                }
            ],
            nodeName: '旱田2号',
            nodeID: '2c99c55674e7c2410174e87761030958',
            nodeLevel: 1,
            nodeParentID: null,
            nodeParentName: null
        },
        {
            id: '2c99c55674e8f7ec0174e920892a0225',
            gridName: '旱田2-1号',
            gridCode: 'WG000100100001',
            gridFullname: '旱田2号->旱田2-1号',
            superiorGridid: '2c99c55674e7c2410174e87761030958',
            superiorGridname: '旱田2号',
            gridLevel: 2,
            fidSize: 1212,
            fidPlant: '001',
            fidPlantName: '水稻',
            fidType: '001',
            fidTypeName: '旱田',
            fidAddr: '',
            projectId: null,
            gridPos: [
                {
                    gridId: '2c99c55674e8f7ec0174e920892a0225',
                    gridCode: 'WG000100100001',
                    posIndex: null,
                    posX: 139.1212,
                    posY: 21.2121,
                    id: '2c99c55674e9212c0174e92c09ac0090'
                },
                {
                    gridId: '2c99c55674e8f7ec0174e920892a0225',
                    gridCode: 'WG000100100001',
                    posIndex: null,
                    posX: 139.1212,
                    posY: 21.2121,
                    id: '2c99c55674e9212c0174e92c0988008e'
                },
                {
                    gridId: '2c99c55674e8f7ec0174e920892a0225',
                    gridCode: 'WG000100100001',
                    posIndex: null,
                    posX: 139.1312,
                    posY: 21.7121,
                    id: '2c99c55674e9212c0174e92c099a008f'
                },
                {
                    gridId: '2c99c55674e8f7ec0174e920892a0225',
                    gridCode: 'WG000100100001',
                    posIndex: null,
                    posX: 139.1212,
                    posY: 21.2121,
                    id: '2c99c55674e8f7ec0174e920893b0226'
                },
                {
                    gridId: '2c99c55674e8f7ec0174e920892a0225',
                    gridCode: 'WG000100100001',
                    posIndex: null,
                    posX: 139.1312,
                    posY: 21.7121,
                    id: '2c99c55674e8f7ec0174e920894c0227'
                },
                {
                    gridId: '2c99c55674e8f7ec0174e920892a0225',
                    gridCode: 'WG000100100001',
                    posIndex: null,
                    posX: 139.1212,
                    posY: 21.2121,
                    id: '2c99c55674e8f7ec0174e920895c0228'
                }
            ],
            nodeName: '旱田2-1号',
            nodeID: '2c99c55674e8f7ec0174e920892a0225',
            nodeLevel: 2,
            nodeParentID: '2c99c55674e7c2410174e87761030958',
            nodeParentName: '旱田2号'
        },
        {
            id: '2c99c55674e93ff50174e966108101fb',
            gridName: '水田1号',
            gridCode: 'WG00010011',
            gridFullname: '水田1号',
            superiorGridid: null,
            superiorGridname: null,
            gridLevel: 1,
            fidSize: 30211.6,
            fidPlant: '',
            fidPlantName: null,
            fidType: '',
            fidTypeName: null,
            fidAddr: '',
            projectId: null,
            gridPos: [
                {
                    gridId: '2c99c55674e93ff50174e966108101fb',
                    gridCode: 'WG00010011',
                    posIndex: null,
                    posX: 119.515168,
                    posY: 32.204765,
                    id: '2c99c55674e93ff50174e966109201fc'
                },
                {
                    gridId: '2c99c55674e93ff50174e966108101fb',
                    gridCode: 'WG00010011',
                    posIndex: null,
                    posX: 119.518084,
                    posY: 32.205007,
                    id: '2c99c55674e93ff50174e96610a201fd'
                },
                {
                    gridId: '2c99c55674e93ff50174e966108101fb',
                    gridCode: 'WG00010011',
                    posIndex: null,
                    posX: 119.517284,
                    posY: 32.206207,
                    id: '2c99c55674e93ff50174e96610b301fe'
                },
                {
                    gridId: '2c99c55674e93ff50174e966108101fb',
                    gridCode: 'WG00010011',
                    posIndex: null,
                    posX: 119.51569,
                    posY: 32.206094,
                    id: '2c99c55674e93ff50174e96610c701ff'
                }
            ],
            nodeName: '水田1号',
            nodeID: '2c99c55674e93ff50174e966108101fb',
            nodeLevel: 1,
            nodeParentID: null,
            nodeParentName: null
        },
        {
            id: '2c99c55674e977e40174e97d861a006b',
            gridName: '水田2号',
            gridCode: 'WG00010012',
            gridFullname: '水田2号',
            superiorGridid: null,
            superiorGridname: null,
            gridLevel: 1,
            fidSize: 10317.1,
            fidPlant: '',
            fidPlantName: null,
            fidType: '',
            fidTypeName: null,
            fidAddr: '',
            projectId: null,
            gridPos: [
                {
                    gridId: '2c99c55674e977e40174e97d861a006b',
                    gridCode: 'WG00010012',
                    posIndex: null,
                    posX: 119.51422,
                    posY: 32.204579,
                    id: '2c99c55674e977e40174e97d8648006c'
                },
                {
                    gridId: '2c99c55674e977e40174e97d861a006b',
                    gridCode: 'WG00010012',
                    posIndex: null,
                    posX: 119.51582,
                    posY: 32.204579,
                    id: '2c99c55674e977e40174e97d865c006d'
                },
                {
                    gridId: '2c99c55674e977e40174e97d861a006b',
                    gridCode: 'WG00010012',
                    posIndex: null,
                    posX: 119.51502,
                    posY: 32.205779,
                    id: '2c99c55674e977e40174e97d866f006e'
                }
            ],
            nodeName: '水田2号',
            nodeID: '2c99c55674e977e40174e97d861a006b',
            nodeLevel: 1,
            nodeParentID: null,
            nodeParentName: null
        }
    ]
};
export default obj;
