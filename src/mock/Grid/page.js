import { page } from '@api/grid';
const { url, mocked, type } = page;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    data: {
        page: 1,
        size: 1,
        total: 6,
        list: [
            {
                id: '2c99c55674e7c2410174e876fcef094f',
                gridName: '旱田1号',
                gridCode: 'WG00010009',
                gridFullname: '旱田1号',
                superiorGridid: null,
                superiorGridname: null,
                gridLevel: 1,
                fidSize: 10207.9,
                fidPlant: '',
                fidPlantName: null,
                fidType: '',
                fidTypeName: null,
                fidAddr: '',
                projectId: null,
                gridPos: [
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.121212,
                        posY: 29.2121,
                        id: '2c99c55674ebff330174ec32cd170320'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.1321212,
                        posY: 29.2211,
                        id: '2c99c55674ebff330174ec32cd270321'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.122212,
                        posY: 29.2221,
                        id: '2c99c55674ebff330174ec32cd370322'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.121212,
                        posY: 29.2121,
                        id: '2c99c55674ebff330174ec315adb0301'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.1321212,
                        posY: 29.2211,
                        id: '2c99c55674ebff330174ec315aec0302'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.122212,
                        posY: 29.2221,
                        id: '2c99c55674ebff330174ec315b040303'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.121212,
                        posY: 29.2121,
                        id: '2c99c55674ebff330174ec2e0dd502b8'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.1321212,
                        posY: 29.2211,
                        id: '2c99c55674ebff330174ec2e0de602b9'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.122212,
                        posY: 29.2221,
                        id: '2c99c55674ebff330174ec2e0df702ba'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.121212,
                        posY: 29.2121,
                        id: '2c99c55674ebff330174ec2d947e02ab'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.1321212,
                        posY: 29.2211,
                        id: '2c99c55674ebff330174ec2d949202ac'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 139.122212,
                        posY: 29.2221,
                        id: '2c99c55674ebff330174ec2d94a302ad'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 119.519251,
                        posY: 32.203143,
                        id: '2c99c55674e7c2410174e876fcff0950'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 119.520851,
                        posY: 32.203143,
                        id: '2c99c55674e7c2410174e876fd110951'
                    },
                    {
                        gridId: '2c99c55674e7c2410174e876fcef094f',
                        gridCode: 'WG00010009',
                        posIndex: null,
                        posX: 19.520051,
                        posY: 32.204343,
                        id: '2c99c55674e7c2410174e876fd220952'
                    }
                ],
                nodeName: '旱田1号',
                nodeID: '2c99c55674e7c2410174e876fcef094f',
                nodeLevel: 1,
                nodeParentID: null,
                nodeParentName: null
            }
        ]
    },
    msg: 'ok'
};
export default obj;
