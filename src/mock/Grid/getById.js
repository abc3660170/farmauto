import { getById } from '@api/grid';
import Mock from 'mockjs';
const Random = Mock.Random;
const { url, mocked, type } = getById;
const name = `${type}|${url}|${mocked}`;
const obj = {};
const poss = [
    [
        { posX: 119.688945, posY: 32.474137 },
        { posX: 119.526306, posY: 32.446383 },
        { posX: 119.647829, posY: 32.326788 },
        { posX: 119.879909, posY: 32.408593 }
    ],
    [
        { posX: 119.425799, posY: 32.402422 },
        { posX: 119.301535, posY: 32.242592 },
        { posX: 119.462347, posY: 32.091769 },
        { posX: 119.700823, posY: 32.135108 },
        { posX: 119.686204, posY: 32.227907 },
        { posX: 119.571991, posY: 32.277361 },
        { posX: 119.636864, posY: 32.343001 }
    ],
    [
        { posX: 119.677446, posY: 32.13953 },
        { posX: 119.695269, posY: 32.135049 },
        { posX: 119.694545, posY: 32.122315 },
        { posX: 119.691983, posY: 32.125145 },
        { posX: 119.687137, posY: 32.12505 }
    ]
];
obj[name] = {
    code: 0,
    data: {
        id: '2c99c55674e977e40174e97d861a006b',
        createTime: '2020-10-02 21:25:52',
        updateTime: '2020-10-02 21:25:52',
        remark: null,
        createBy: null,
        deleteFlag: false,
        projectId: '2c823f84ef4011eaab5700163e101bca',
        gridCode: 'WG00010012',
        gridName: '水田2号',
        superiorGridid: null,
        gridLevel: 1,
        gridFullname: '水田2号',
        gridLeaderTel: null,
        gridLeaderID: null,
        gridLayoutPicture: null,
        gridcenterX: 119.51502,
        gridcenterY: 32.204979,
        superiorGridname: null,
        gridPathIds: null,
        fidSize: 10317.1,
        fidPlant: '001',
        fidPlantName: '水稻',
        fidType: '',
        fidTypeName: null,
        fidAddr: '',
        gridPos: () => {
            return Random.pick(poss);
        }
    },
    msg: 'ok'
};
export default obj;
