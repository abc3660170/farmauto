import { saveDevice } from '@api/devicebill';
const { url, mocked, type } = saveDevice;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    data: {},
    msg: 'ok'
};
export default obj;
