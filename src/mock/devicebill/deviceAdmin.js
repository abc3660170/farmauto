import { deviceAdmin } from '@api/devicebill';
const { url, mocked, type } = deviceAdmin;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '获取数据字典成功!',
    data: [
        {
            id: '001',
            label: '宋义进'
        },
        {
            id: '002',
            label: '姜成禄'
        },
        {
            id: '003',
            label: '简自豪'
        },
        {
            id: '004',
            label: '喻文波'
        },
        {
            id: '005',
            label: '刘世宇'
        }
    ]
};
export default obj;
