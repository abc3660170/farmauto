import { machCount } from '@api/devicebill';
const { url, mocked, type } = machCount;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '设备清单统计值',
    data: [
        {
            name: '02',
            label: '空闲',
            num: 56
        },
        {
            name: '03',
            label: '维修中',
            num: 18
        },
        {
            name: '04',
            label: '保养中',
            num: 6
        },
        {
            name: '01',
            label: '在用',
            num: 10
        }
    ]
};
export default obj;
