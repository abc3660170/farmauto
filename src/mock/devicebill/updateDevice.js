import { updateDevice } from '@api/devicebill';
const { url, mocked, type } = updateDevice;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    data: {},
    msg: 'ok'
};
export default obj;
