import { deleteDevice } from '@api/devicebill';
const { url, mocked, type } = deleteDevice;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    data: {},
    msg: 'ok'
};
export default obj;
