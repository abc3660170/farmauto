import { setScore } from '@api/jobbill';
const { url, mocked, type } = setScore;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '评价成功',
    data: true
};
export default obj;
