import { update } from '@api/jobbill';
const { url, mocked, type } = update;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '发布任务成功',
    data: true
};
export default obj;
