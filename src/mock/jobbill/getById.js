import { getById } from '@api/jobbill';
const { url, mocked, type } = getById;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '获取指定ID的作业清单对象成功',
    data: {
        id: '2c99c55674b911f90174ba7bc79e0006',
        createTime: '2020-09-23 18:21:48',
        updateTime: '2020-09-23 18:21:48',
        remark: null,
        createBy: null,
        deleteFlag: false,
        jobName: '插秧',
        jobCode: '01',
        jobType: '02',
        jobTypeName: null,
        requirement:
            '插秧作业注意事项1、放秧人员在放秧时一定要注意：秧盘一定要先用双手拿住外圈顶边，将其全部放开再放入秧台，也就是放秧不能刺头放秧，应顺头朝上放下，避免猪爪秧。2、压秧杆要根据秧土的厚薄层度适时调整，压杆调整是以秧放进秧台后，手能从秧土和压杆之间插进去为合适，总体秧土不能太薄，土太薄、盘根再不足，秧块在秧台上易弓腰也会出现猪爪秧和断苗现象。3、取秧量调整以横向调整为主，纵向调整幅度不宜太大，横向调整一般放在20次档，钵形毯状苗要求放在18次档。4、各部位调整：（1）插秧丛数调节：通过切换苗距调节手柄来进行调节，分6级，苗距越窄插秧丛数越多，苗距越宽则插秧丛数越少；（2）取苗量调节：可用横向传送切换手柄和纵向取苗量调节手柄来调节，横向共3档：15次档适用成苗、18次档适用中苗、20次档适用幼苗，三叶一心苗一般选用20次档，纵向取苗量可根据秧苗成苗床情况进行调节可在8～18毫米范围内进行10个阶段调节，通过调节使每穴的苗数符合农艺要求。5、插秧机秧针磨损后要及时更换，久保田插秧机秧针从针尖到下紧固螺丝中心线距离是83mm,小于或等于80mm必须更换秧针。6、正确调整好取秧角度，避免出现从秧苗中下部取苗插入土中，导致栽后黄秧死苗（猪爪秧）。7、取秧量不能太大（4～5苗为宜），太大会导致出现断苗现象。 ',
        dispatchTime: '2020-09-23 00:00:00',
        acceptTime: '2020-09-23 00:00:00',
        startTime: '2020-09-20 00:00:00',
        endTime: '2020-09-23 00:00:00',
        jobStatus: '01',
        jobStatusName: '未完成',
        projectId: '2c823f84ef4011eaab5700163e101bca',
        userApplied: [
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                jobName: null,
                driverId: '2c99c55674aadab70174aff49dd4001e',
                driverName: null,
                dispatchTime: null,
                acceptTime: null,
                jobStatus: '',
                jobStatusName: '待接单',
                phone: '15951475896',
                username: '简自豪',
                id: '2c99c55674ebff330174eca99b290b0e'
            },
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                jobName: null,
                driverId: '2c99c55674aadab70174b33a51170027',
                driverName: null,
                dispatchTime: '2020-09-23 00:00:00',
                acceptTime: '2020-09-23 00:00:00',
                jobStatus: '01',
                jobStatusName: '待接单',
                phone: '15951475896',
                username: '金咕咕',
                id: '2c99c55674bac8630174bad879000000'
            },
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                jobName: null,
                driverId: '2c99c55674aadab70174b33a51170027',
                driverName: null,
                dispatchTime: '2020-09-23 00:00:00',
                acceptTime: '2020-09-23 00:00:00',
                jobStatus: '01',
                jobStatusName: '已接单',
                phone: '15951475896',
                username: '宋义进',
                id: '2c99c55674bac8630174bad879000000'
            },
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                jobName: null,
                driverId: '2c99c55674aadab70174b33a51170027',
                driverName: null,
                dispatchTime: '2020-09-23 00:00:00',
                acceptTime: '2020-09-23 00:00:00',
                jobStatus: '01',
                jobStatusName: '已接单',
                phone: '15951475896',
                username: '姜成禄',
                id: '2c99c55674bac8630174bad879000000'
            },
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                jobName: null,
                driverId: '2c99c55674aadab70174b33a51170027',
                driverName: null,
                dispatchTime: '2020-09-23 00:00:00',
                acceptTime: '2020-09-23 00:00:00',
                jobStatus: '006',
                jobStatusName: '已拒单',
                phone: '15951475896',
                username: '喻文波',
                id: '2c99c55674bac8630174bad879000000'
            },
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                jobName: null,
                driverId: '2c99c55674aadab70174b33a51170027',
                driverName: null,
                dispatchTime: '2020-09-23 00:00:00',
                acceptTime: '2020-09-23 00:00:00',
                jobStatus: '01',
                jobStatusName: '进行中',
                phone: '15951475896',
                username: '李相赫',
                id: '2c99c55674bac8630174bad879000000'
            },
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                jobName: null,
                driverId: '2c99c55674aadab70174b33a51170027',
                driverName: null,
                dispatchTime: '2020-09-23 00:00:00',
                acceptTime: '2020-09-23 00:00:00',
                jobStatus: '01',
                jobStatusName: '已完成',
                phone: '15951475896',
                username: '刘世宇',
                id: '2c99c55674bac8630174bad879000000'
            }
        ],
        machApplied: [
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                machineId: '4028098174bdab9e0174bdb4898d0000',
                monitorId: '8a8348e574955e8a01749565adc00000',
                jobName: null,
                machineName: '播种机',
                toolName: null,
                monitorName: null,
                id: '4028098174dc6fb90174dc810e820000'
            }
        ],
        toolApplied: [
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                toolId: '2c99c55674c2b3270174c578874b0012',
                monitorId: '8a8348e574955e8a01749565adc00000',
                jobName: null,
                machineName: null,
                toolName: '锄头',
                monitorName: null,
                id: '4028098174dc6fb90174dc810e820000'
            }
        ],
        fieldApplied: [
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                fieldId: '2c99c55674bac8630174bada0d290001',
                jobName: null,
                fieldName: '秧地',
                id: '4028098174dd0dd40174dd124d0f0000'
            },
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                fieldId: '2c99c55674bac8630174bada0d290001',
                jobName: null,
                fieldName: '水稻田',
                id: '4028098174dcfd1e0174dd007aae0000'
            }
        ],
        jobSchedule: [
            {
                jobId: '2c99c55674b911f90174ba7bc79e0006',
                days: '0',
                startDate: '2020-09-24 00:00:00',
                endDate: '2020-09-24 00:00:00',
                startTime: '1970-01-01 00:00:00',
                endTime: '1970-01-01 00:00:00',
                projectId: null,
                id: '4028098174bd748d0174bd8cdc8f0000'
            }
        ]
    }
};
export default obj;
