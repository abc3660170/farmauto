import { queryField } from '@api/jobbill';
const { url, mocked, type } = queryField;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '通过ID或全名获取网格对象成功',
    data: {
        id: '000',
        labelName: '全部地块',
        children: [
            {
                id: '001',
                labelName: '水稻',
                children: [
                    {
                        id: '2c99c55674e7c2410174e877c6a70961',
                        labelName: '旱田1-1号'
                    },
                    {
                        id: '2c99c55674e8f7ec0174e920892a0225',
                        labelName: '旱田2-1号'
                    }
                ]
            },
            {
                id: '002',
                labelName: '玉米',
                children: [
                    {
                        id: '2c99c55674e977e40174e97d861a006b',
                        labelName: '水田2号'
                    }
                ]
            }
        ]
    }
};

export default obj;
