import { page } from '@api/jobbill';
const { url, mocked, type } = page;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '作业清单对象分页查询成功',
    data: {
        page: 1,
        size: 2,
        total: 12,
        list: [
            {
                jobName: '收获作业1',
                jobCode: '001',
                jobType: '002',
                jobTypeName: '收获作业',
                requirement: '作业要求',
                dispatchTime: '2020-10-02 11:45:12',
                acceptTime: '2020-10-02 11:45:12',
                endTime: '2020-10-02 11:45:12',
                jobStatus: '002',
                jobStatusName: '已接单',
                projectId: '2c823f84ef4011eaab5700163e101bca',
                userAppliedNames: null,
                id: '4028ca817505b376017505b83e2b0000',
                participateNum: 10,
                createTime: '2020-09-18 12:09',
                userStaus: [
                    { id: '002', name: '已接单', count: 5 },
                    { id: '005', name: '待接单', count: 2 },
                    { id: '006', name: '已拒单', count: 3 },
                    { id: '003', name: '已开始', count: 5 },
                    { id: '004', name: '已结束', count: 5 }
                ],
                startTime: '2020-09-18 12:09',
                score: 3,
                scoreDesc: '我就是测试一下'
            },
            {
                jobName: '定位作业2',
                jobCode: '1111',
                jobType: '001',
                jobTypeName: '定位作业',
                requirement: '定位作业2要求',
                dispatchTime: '2020-09-29 00:00:00',
                acceptTime: '2020-09-29 00:00:00',
                endTime: '2020-09-29 00:00:00',
                jobStatus: '002',
                jobStatusName: '已接单',
                projectId: '2c823f84ef4011eaab5700163e101bca',
                userAppliedNames: null,
                id: '2c99c55674d8f6ba0174d9b0b6240005',
                participateNum: 10,
                createTime: '2020-09-18 12:09',
                userStaus: [
                    { id: '002', name: '已接单', count: 5 },
                    { id: '005', name: '待接单', count: 1 },
                    { id: '006', name: '已拒单', count: 3 },
                    { id: '003', name: '已开始', count: 3 },
                    { id: '004', name: '已结束', count: 7 }
                ],
                startTime: '2020-09-18 12:09',
                score: '',
                scoreDesc: ''
            },
            {
                jobName: '定位作业3',
                jobCode: '1111',
                jobType: '001',
                jobTypeName: '定位作业',
                requirement: '定位作业2要求',
                dispatchTime: '2020-09-29 00:00:00',
                acceptTime: '2020-09-29 00:00:00',
                endTime: '2020-09-29 00:00:00',
                jobStatus: '002',
                jobStatusName: '已接单',
                projectId: '2c823f84ef4011eaab5700163e101bca',
                userAppliedNames: null,
                id: '2c99c55674d8f6ba0174d9b0b6240005',
                participateNum: 10,
                createTime: '2020-09-18 12:09',
                userStaus: [
                    { id: '002', name: '已接单', count: 8 },
                    { id: '005', name: '待接单', count: 1 },
                    { id: '006', name: '已拒单', count: 0 },
                    { id: '003', name: '已开始', count: 1 },
                    { id: '004', name: '已结束', count: 9 }
                ],
                startTime: '2020-09-18 12:09',
                score: '',
                scoreDesc: ''
            },
            {
                jobName: 'string',
                jobCode: 'string',
                jobType: '001',
                jobTypeName: '定位作业',
                requirement: 'string',
                dispatchTime: '2020-10-04 12:47:38',
                acceptTime: '2020-10-04 12:47:38',
                startTime: '2020-10-04 12:47:38',
                endTime: '2020-10-04 12:47:38',
                jobStatus: '007',
                jobStatusName: '待发布',
                participateNum: 6,
                score: '',
                scoreDesc: '',
                userStaus: [
                    { id: '002', name: '已接单', count: 6 },
                    { id: '005', name: '待接单', count: 1 },
                    { id: '006', name: '已拒单', count: 0 },
                    { id: '003', name: '已开始', count: 5 },
                    { id: '004', name: '已结束', count: 1 }
                ],
                projectId: '2c823f84ef4011eaab5700163e101bca',
                createTime: null,
                userAppliedNames: null,
                id: '2c99c55674f835ab0174f83724cf0000'
            }
        ]
    }
};
export default obj;
