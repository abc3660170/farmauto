import { getCount } from '@api/jobbill';
const { url, mocked, type } = getCount;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '获取指定ID的作业清单对象成功',
    data: [
        { id: '007', name: '待发布', count: 10 },
        { id: '008', name: '进行中', count: 12 },
        { id: '009', name: '已完成', count: 11 }
    ]
};
export default obj;
