import { save } from '@api/jobbill';
const { url, mocked, type } = save;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '新建作业清单对象成功',
    data: true
};
export default obj;
