import { redispatch } from '@api/jobbill';
const { url, mocked, type } = redispatch;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '重新派单成功',
    data: true
};
export default obj;
