import { monitorOnOff } from '@api/monitorData';
const { url, mocked, type } = monitorOnOff;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    data: {},
    msg: 'ok'
};
export default obj;
