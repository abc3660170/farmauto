import { listDbByJob } from '@api/monitorData';
const { url, mocked, type } = listDbByJob;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    data: {
        timexAxis: ['8:00', '10:00', '12:00', '14:00', '16:00l'],
        list: [
            {
                id: '@increment', //农机id
                machineName: '拖拉机@increment',
                oilChange: [200, 180, 130, 40, 20], // 24小时变化
                path: [
                    [119.688945, 32.474137],
                    [119.526306, 32.446383],
                    [119.647829, 32.326788],
                    [119.879909, 32.408593],
                    [119.688945, 32.474137]
                ], //路径坐标
                markers: [
                    {
                        markerId: '@increment', // 单个点标记的id 方便映射到点上和图片上
                        pos: [119.526306, 32.446383],
                        pic1: require('@assets/image/guidance/pic1.png'),
                        pic2: require('@assets/image/guidance/pic1.png'),
                        time: ''
                    },
                    {
                        markerId: '@increment',
                        pos: [119.647829, 32.326788],
                        pic1: require('@assets/image/guidance/pic2.png'),
                        pic2: require('@assets/image/guidance/pic2.png'),
                        time: ''
                    },
                    {
                        markerId: '@increment',
                        pos: [119.879909, 32.408593],
                        pic1: require('@assets/image/guidance/pic3.png'),
                        pic2: require('@assets/image/guidance/pic3.png'),
                        time: ''
                    }
                ] //关键点集合
            },
            {
                id: '@increment', //农机id
                machineName: '拖拉机@increment',
                oilChange: [20, 200, 180, 160, 120], // 24小时变化
                path: [
                    [118.688945, 32.474137],
                    [118.526306, 32.446383],
                    [118.647829, 32.326788],
                    [118.879909, 32.408593],
                    [118.688945, 32.474137]
                ], //路径坐标
                markers: [
                    {
                        markerId: '@increment',
                        pos: [118.688945, 32.474137],
                        pic1: require('@assets/image/guidance/pic4.png'),
                        pic2: require('@assets/image/guidance/pic4.png'),
                        time: ''
                    },
                    {
                        markerId: '@increment',
                        pos: [118.647829, 32.326788],
                        pic1: require('@assets/image/guidance/pic4.png'),
                        pic2: require('@assets/image/guidance/pic4.png'),
                        time: ''
                    },
                    {
                        markerId: '@increment',
                        pos: [118.879909, 32.408593],
                        pic1: require('@assets/image/guidance/pic1.png'),
                        pic2: require('@assets/image/guidance/pic1.png'),
                        time: ''
                    }
                ] //关键点集合
            }
        ]
    },
    msg: 'ok'
};
export default obj;
