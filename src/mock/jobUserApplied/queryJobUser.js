import { queryJobUser } from '@api/jobUserApplied';
const { url, mocked, type } = queryJobUser;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '获取所有作业关联人员成功',
    data: [
        {
            jobId: '2c99c55674b911f90174ba7bc79e0006',
            jobName: 'string',
            driverId: '2c99c55674aadab70174b33a51170027',
            driverName: '天1',
            userMobile: '137',
            dispatchTime: null,
            acceptTime: null,
            jobStatus: '001',
            jobStatusName: '已派单',
            id: '4028ca8174fd20ce0174fd21f5d90000'
        }
    ]
};
export default obj;
