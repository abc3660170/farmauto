// 首先引入Mock
import { setup, mock } from 'mockjs';
import { apiBaseUrl as baseUrl } from '@config/base';
// 设置拦截ajax请求的相应时间
setup({
    timeout: '200-600'
});

/*******************************************************
 *
 * 可以在这里补充一些全局自定义mock数据
 *
 *******************************************************/

let configArray = [];
// 使用webpack的require.context()遍历所有mock文件
const files = require.context('.', true, /\.js$/);
files.keys().forEach(key => {
    if (key === './index.js') return;
    configArray = configArray.concat(files(key).default);
});
//import log from '@utils/log';
// 注册所有的mock服务
configArray.forEach(item => {
    for (const [key, target] of Object.entries(item)) {
        const protocol = key.split('|');
        const requestUrl = `${baseUrl}${protocol[1]}`;
        //log.info(requestUrl);
        const suitUrl = requestUrl.replace(/\//g, '\\/');
        const regStr = `/^${suitUrl}\\?|${suitUrl}$/`;
        if (protocol[2] === 'true') mock(eval(regStr), protocol[0], target);
    }
});
