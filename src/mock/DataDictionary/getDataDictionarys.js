import { getDataDictionarys } from '@api/dictionary';
const { url, mocked, type } = getDataDictionarys;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '获取数据字典成功!',
    data: [
        {
            typename: 'db_planttype',
            count: 4,
            dictionaryList: [
                {
                    id: '001',
                    name: '水稻'
                },
                {
                    id: '002',
                    name: '玉米'
                },
                {
                    id: '003',
                    name: '高粱'
                },
                {
                    id: '004',
                    name: '大豆'
                }
            ]
        },
        {
            typename: 'db_fieldtype',
            count: 2,
            dictionaryList: [
                {
                    id: '001',
                    name: '旱田'
                },
                {
                    id: '002',
                    name: '水田'
                }
            ]
        }
    ]
};
export default obj;
