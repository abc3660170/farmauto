import { list } from '@api/jobMachApplied';
const { url, mocked, type } = list;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '获取所有网格信息成功',
    'data|5-8': [
        {
            jobId: '2c99c55674b911f90174ba7bc79e0006',
            machineId: '@increment(1000)',
            monitorId: '2c99c55674f2b2820174f2d472ae02a0',
            jobName: 'string',
            machineName: '拖拉机2@increment(1000)',
            monitorName: '监控1',
            hostname: 'ujsiot-00003',
            cameraList: [
                {
                    switches: false,
                    url: 'https://tx1.yunchuanglive.com/live/SSAA-349354-DBDDE.m3u8'
                },
                {
                    switches: true,
                    url: 'https://tx1.yunchuanglive.com/live/SSAA-349316-FEFAB.m3u8'
                }
            ],
            id: '4028098174dc6fb90174dc810e820000'
        }
    ]
};
export default obj;
