import { getMachMergelTree } from '@api/devicecategory';
const { url, mocked, type } = getMachMergelTree;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '通过ID或全名获取网格对象成功',
    data: {
        id: '000',
        label: '全部农机',
        count: 3,
        children: [
            {
                id: '001',
                label: '水稻',
                count: 2,
                children: [
                    {
                        id: '2c99c55674e7c2410174e877c6a70961',
                        gridName: '收割机',
                        label: '收割机'
                    },
                    {
                        id: '2c99c55674e8f7ec0174e920892a0225',
                        gridName: '插秧机',
                        label: '插秧机'
                    }
                ]
            },
            {
                id: '002',
                label: '玉米',
                count: 1,
                children: [
                    {
                        id: '2c99c55674e977e40174e97d861a006b',
                        gridName: '播种机',
                        label: '播种机'
                    }
                ]
            }
        ]
    }
};
export default obj;
