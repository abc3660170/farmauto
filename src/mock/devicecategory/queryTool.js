import { getToolMergelTree } from '@api/devicecategory';
const { url, mocked, type } = getToolMergelTree;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '通过ID或全名获取网格对象成功',
    data: {
        id: '000',
        label: '全部农具',
        count: 3,
        children: [
            {
                id: '001',
                label: '除草',
                count: 2,
                children: [
                    {
                        id: '2c99c55674e7c2410174e877c6a70961',
                        gridName: '锄头',
                        label: '锄头'
                    },
                    {
                        id: '2c99c55674e8f7ec0174e920892a0225',
                        gridName: '铲子',
                        label: '铲子'
                    }
                ]
            },
            {
                id: '002',
                label: '打农药',
                count: 1,
                children: [
                    {
                        id: '2c99c55674e977e40174e97d861a006b',
                        gridName: '喷壶',
                        label: '喷壶'
                    }
                ]
            }
        ]
    }
};
export default obj;
