import { getMachDevicecategoryTree } from '@api/devicecategory';
const { url, mocked, type } = getMachDevicecategoryTree;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '依据农机分类ID获取农机分类全路径ID列表成功',
    data: [
        {
            id: '8a8348e5748ab8d601748abb49c20001',
            value: '8a8348e5748ab8d601748abb49c20001',
            level: 1,
            superiorId: 'null',
            superiorName: null,
            fullName: '农机',
            name: '农机',
            label: '农机',
            children: [
                {
                    id: '001',
                    label: '除草',
                    children: [
                        {
                            id: '0011',
                            label: '日本除草机'
                        },
                        {
                            id: '0012',
                            label: '中国除草机'
                        }
                    ]
                },
                {
                    id: '002',
                    label: '收割',
                    count: 1
                }
            ]
        }
    ]
};
export default obj;
