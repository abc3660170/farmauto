import { employ } from '@api/user';
const { url, mocked, type } = employ;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '聘用成功',
    data: true
};
export default obj;
