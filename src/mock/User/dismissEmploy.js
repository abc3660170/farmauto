import { dismissEmploy } from '@api/user';
const { url, mocked, type } = dismissEmploy;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '解聘成功',
    data: true
};
export default obj;
