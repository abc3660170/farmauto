import { login } from '@api/login';
const { url, mocked, type } = login;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '登录成功',
    data: {
        id: '1e72c5b9f4d511eaab5700163e101bca',
        userName: '测试',
        userLoginname: 'super',
        userType: '1',
        userPicpath: '/system/basic/a51c4dc6b2bb42d7906c0b3e126c70c8.jpeg',
        roleId: '002',
        roleName: '系统管理员',
        projectId: '2c823f84ef4011eaab5700163e101bca',
        projectName: 'test',
        projectLogo: null,
        token:
            'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwibmFtZSI6InN1cGVyIiwiZXhwIjoxNjAxOTQ5MDQ5LCJpYXQiOjE2MDE4NjI2NDl9.T3_ojDENtYim2V7hwdRAam8u9KfwP2mZjUbG-_xdzk0',
        roleIds: ['002']
    }
};
export default obj;
