import { updateDriver } from '@api/user';
const { url, mocked, type } = updateDriver;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '修改成功',
    data: true
};
export default obj;
