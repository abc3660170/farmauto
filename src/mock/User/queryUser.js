import { queryUser } from '@api/user';
const { url, mocked, type } = queryUser;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '获取可选人员成功',
    data: [
        { id: '001', userName: '姜成禄' },
        { id: '002', userName: '金咕咕' },
        { id: '003', userName: '宋义进' }
    ]
};
export default obj;
