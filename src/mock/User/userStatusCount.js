import { userStatusCount } from '@api/user';
const { url, mocked, type } = userStatusCount;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '农机手统计值',
    data: [
        {
            name: '001',
            label: '未聘用',
            num: 20
        },
        {
            name: '002',
            label: '聘用中',
            num: 56
        },
        {
            name: '003',
            label: '聘用到期',
            num: 15
        },
        {
            name: '004',
            label: '解聘',
            num: 6
        }
    ]
};
export default obj;
