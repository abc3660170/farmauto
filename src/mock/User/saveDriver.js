import { saveDriver } from '@api/user';
const { url, mocked, type } = saveDriver;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '添加成功',
    data: true
};
export default obj;
