import { delPersonnel } from '@api/user';
const { url, mocked, type } = delPersonnel;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '删除成功',
    data: true
};
export default obj;
