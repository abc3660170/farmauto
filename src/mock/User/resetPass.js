import { resetPass } from '@api/user';
const { url, mocked, type } = resetPass;
const name = `${type}|${url}|${mocked}`;
const obj = {};
obj[name] = {
    code: 0,
    msg: '重置密码成功',
    data: true
};
export default obj;
