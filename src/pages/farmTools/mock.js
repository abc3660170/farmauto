export const treeNode = [
    {
        label: '一级 1',
        children: [
            {
                label: '二级 1-1',
                children: [
                    {
                        label: '三级 1-1-1'
                    }
                ]
            }
        ]
    },
    {
        label: '一级 2',
        children: [
            {
                label: '二级 2-1',
                children: [
                    {
                        label: '三级 2-1-1'
                    }
                ]
            },
            {
                label: '二级 2-2',
                children: [
                    {
                        label: '三级 2-2-1'
                    }
                ]
            }
        ]
    },
    {
        label: '一级 3',
        children: [
            {
                label: '二级 3-1',
                children: [
                    {
                        label: '三级 3-1-1'
                    }
                ]
            },
            {
                label: '二级 3-2',
                children: [
                    {
                        label: '三级 3-2-1'
                    }
                ]
            }
        ]
    }
];
export const farmToolsData = [
    {
        number: '01216546161',
        name: '农机设备1',
        brandInfo: '品牌信息',
        classify: '分类信息',
        addTime: '2020-09-18 12：09',
        status: 'upkeep',
        admin: '张杰'
    },
    {
        number: '01216546163',
        name: '农机设备1',
        brandInfo: '品牌信息',
        classify: '分类信息',
        addTime: '2020-09-18 12：09',
        status: 'maintain',
        admin: '张杰'
    },
    {
        number: '012165461644',
        name: '农机设备1',
        brandInfo: '品牌信息',
        classify: '分类信息',
        addTime: '2020-09-18 12：09',
        status: 'normal',
        admin: '张杰'
    },
    {
        number: '012165676164',
        name: '农机设备1',
        brandInfo: '品牌信息',
        classify: '分类信息',
        addTime: '2020-09-18 12：09',
        status: 'operator',
        admin: '张杰'
    },
    {
        number: '01216577164',
        name: '农机设备1',
        brandInfo: '品牌信息',
        classify: '分类信息',
        addTime: '2020-09-18 12：09',
        status: 'upkeep',
        admin: '张杰'
    },
    {
        number: '012168164',
        name: '农机设备1',
        brandInfo: '品牌信息',
        classify: '分类信息',
        addTime: '2020-09-18 12：09',
        status: 'normal',
        admin: '张杰'
    }
];
export const classifyToolsList = [
    {
        label: '农机 1',
        children: [
            {
                label: '农机 1-1',
                children: [
                    {
                        label: '农机 1-1-1'
                    }
                ]
            }
        ]
    },
    {
        label: '农机 2',
        children: [
            {
                label: '农机 2-1',
                children: [
                    {
                        label: '农机 2-1-1'
                    }
                ]
            },
            {
                label: '农机 2-2',
                children: [
                    {
                        label: '农机 2-2-1'
                    }
                ]
            }
        ]
    },
    {
        label: '农机 3',
        children: [
            {
                label: '农机 3-1',
                children: [
                    {
                        label: '农机 3-1-1'
                    }
                ]
            },
            {
                label: '农机 3-2',
                children: [
                    {
                        label: '农机 3-2-1'
                    }
                ]
            }
        ]
    }
];
export const adminList = [
    {
        value: '管理员1'
    },
    {
        value: '管理员2'
    },
    {
        value: '管理员3'
    },
    {
        value: '管理员4'
    },
    {
        value: '管理员5'
    },
    {
        value: '管理员5'
    },
    {
        value: '管理员6'
    }
];
