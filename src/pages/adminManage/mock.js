export const adminList = [
    {
        value: '王林'
    },
    {
        value: '王麻子'
    },
    {
        value: '苏明'
    },
    {
        value: '我欲封天'
    },
    {
        value: '管理员5'
    },
    {
        value: '管理员5'
    },
    {
        value: '管理员6'
    }
];
export const allProjectList = [
    {
        key: 1,
        label: '项目1',
        value: '项目1'
    },
    {
        key: 2,
        label: '项目2',
        value: '项目2'
    },
    {
        key: 3,
        label: '项目3',
        value: '项目3'
    },
    {
        key: 4,
        label: '项目4',
        value: '项目4'
    },
    {
        key: 5,
        label: '项目5',
        value: '项目5'
    },
    {
        key: 6,
        label: '项目6',
        value: '项目6'
    },
    {
        key: 7,
        label: '项目7',
        value: '项目7'
    }
];
export const adminTableData = [
    {
        name: '项目名称',
        realName: '描述信息',
        gender: '男',
        telephone: '15150698654',
        addTime: '2020-09-18 12：09',
        projects: ['项目1', '项目2']
    },
    {
        name: '项目名称',
        realName: '描述信息',
        gender: '男',
        telephone: '15150698654',
        addTime: '2020-09-18 12：09',
        projects: ['项目1', '项目2']
    },
    {
        name: '项目名称',
        realName: '描述信息',
        gender: '男',
        telephone: '15150698654',
        addTime: '2020-09-18 12：09',
        projects: ['项目1']
    },
    {
        name: '项目名称',
        realName: '描述信息',
        gender: '男',
        telephone: '15150698654',
        addTime: '2020-09-18 12：09',
        projects: ['项目1', '项目2', '项目三']
    },
    {
        name: '项目名称',
        realName: '描述信息',
        gender: '男',
        telephone: '15150698654',
        addTime: '2020-09-18 12：09',
        projects: ['项目1', '项目2']
    },
    {
        name: '项目名称',
        realName: '描述信息',
        gender: '男',
        telephone: '15150698654',
        addTime: '2020-09-18 12：09',
        projects: ['项目1', '项目2']
    },
    {
        name: '项目名称',
        realName: '描述信息',
        gender: '男',
        telephone: '15150698654',
        addTime: '2020-09-18 12：09',
        projects: ['项目1', '项目2']
    }
];
