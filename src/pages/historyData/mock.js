export const toolList = [
    { label: '农机名称1' },
    { label: '农机名称2' },
    { label: '农机名称3' },
    { label: '农机名称4' },
    { label: '农机名称5' }
];

export const chartData = {
    xAxis: [
        '12:00',
        '13:00',
        '14:00',
        '15:00',
        '16:00',
        '17:00',
        '18:00',
        '19:00',
        '20:00',
        '21:00'
    ],
    data: [
        {
            label: '农机名称1',
            value: [
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50)
            ]
        },
        {
            label: '农机名称2',
            value: [
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50)
            ]
        },
        {
            label: '农机名称3',
            value: [
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50)
            ]
        },
        {
            label: '农机名称4',
            value: [
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50)
            ]
        },
        {
            label: '农机名称5',
            value: [
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50),
                Math.ceil(Math.random() * 50)
            ]
        }
    ]
};

export const photoList = [
    { imgSrc: require('@assets/image/guidance/pic1.png') },
    { imgSrc: require('@assets/image/guidance/pic2.png') },
    { imgSrc: require('@assets/image/guidance/pic3.png') },
    { imgSrc: require('@assets/image/guidance/pic4.png') }
];
