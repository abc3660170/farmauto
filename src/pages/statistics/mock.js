export const topData = [
    {
        name: '地块总面积',
        img: require('@assets/image/statistics/land.png'),
        num: '1万2千亩'
    },
    {
        name: '农机手总个数',
        img: require('@assets/image/statistics/farmer.png'),
        num: '200人'
    },
    {
        name: '农机总数量',
        img: require('@assets/image/statistics/machinery.png'),
        num: '200个'
    }
];

export const farmerTableData = [
    {
        name: '王小虎',
        time: '1',
        number: '20',
        leaveNum: '20',
        score: '5'
    },
    {
        name: '王小虎',
        time: '1',
        number: '20',
        leaveNum: '20',
        score: '5'
    },
    {
        name: '王小虎',
        time: '1',
        number: '20',
        leaveNum: '20',
        score: '5'
    },
    {
        name: '王小虎',
        time: '1',
        number: '20',
        leaveNum: '20',
        score: '5'
    },
    {
        name: '王小虎',
        time: '1',
        number: '20',
        leaveNum: '20',
        score: '5'
    },
    {
        name: '王小虎',
        time: '1',
        number: '20',
        leaveNum: '20',
        score: '5'
    },
    {
        name: '王小虎',
        time: '1',
        number: '20',
        leaveNum: '20',
        score: '5'
    },
    {
        name: '王小虎',
        time: '1',
        number: '20',
        leaveNum: '20',
        score: '5'
    },
    {
        name: '王小虎',
        time: '1',
        number: '20',
        leaveNum: '20',
        score: '5'
    },
    {
        name: '王小虎',
        time: '1',
        number: '20',
        leaveNum: '20',
        score: '5'
    }
];

export const useFrequencyTableData = [
    {
        type: '农机类型',
        frequency: '100次/1年',
        progress: 100
    },
    {
        type: '农机类型',
        frequency: '80次/1年',
        progress: 80
    },
    {
        type: '农机类型',
        frequency: '70次/1年',
        progress: 70
    },
    {
        type: '农机类型',
        frequency: '70次/1年',
        progress: 70
    },
    {
        type: '农机类型',
        frequency: '70次/1年',
        progress: 70
    },
    {
        type: '农机类型',
        frequency: '70次/1年',
        progress: 70
    },
    {
        type: '农机类型',
        frequency: '70次/1年',
        progress: 70
    },
    {
        type: '农机类型',
        frequency: '70次/1年',
        progress: 70
    },
    {
        type: '农机类型',
        frequency: '70次/1年',
        progress: 70
    },
    {
        type: '农机类型',
        frequency: '70次/1年',
        progress: 70
    }
];

export const pieData = [
    { value: 335, name: '棉花地' },
    { value: 310, name: '分类1' },
    { value: 234, name: '分类2' },
    { value: 135, name: '分类3' },
    { value: 253, name: '分类4' },
    { value: 182, name: '分类5' }
];
