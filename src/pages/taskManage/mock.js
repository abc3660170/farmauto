export const tabData = [
    { label: '待发布(11)', name: 'TaskTodo', id: 0 },
    { label: '进行中(11)', name: 'TaskDoing', id: 1 },
    { label: '已完成(11)', name: 'TaskOver', id: 2 }
];

export const todoTable = {
    thead: [
        { name: 'jobName', label: '作业名称' },
        { name: 'total', label: '需要农机手总数' },
        { name: 'receive', label: '接单农机手数' },
        { name: 'reject', label: '拒单农机手数' },
        { name: 'process', label: '派单进度' },
        { name: 'time', label: '创建时间' }
    ],
    tbody: [
        {
            jobName: 'XXXX作业',
            total: 10,
            receive: 9,
            reject: 1,
            time: '2020-09-18 12:09'
        },
        {
            jobName: 'XXXX作业',
            total: 10,
            receive: 9,
            reject: 0,
            time: '2020-09-18 12:09'
        },
        {
            jobName: 'XXXX作业',
            total: 10,
            receive: 10,
            reject: 0,
            time: '2020-09-18 12:09'
        },
        {
            jobName: 'XXXX作业',
            total: 10,
            receive: 9,
            reject: 0,
            time: '2020-09-18 12:09'
        },
        {
            jobName: 'XXXX作业',
            total: 10,
            receive: 10,
            reject: 0,
            time: '2020-09-18 12:09'
        }
    ]
};

export const doingTable = {
    tbody: [
        {
            name: 'XXXX作业',
            total: 10,
            over: 9,
            ing: 1,
            time: '2020-09-18 12:09'
        },
        {
            name: 'XXXX作业',
            total: 10,
            over: 9,
            ing: 1,
            time: '2020-09-18 12:09'
        },
        {
            name: 'XXXX作业',
            total: 10,
            over: 9,
            ing: 1,
            time: '2020-09-18 12:09'
        },
        {
            name: 'XXXX作业',
            total: 10,
            over: 9,
            ing: 1,
            time: '2020-09-18 12:09'
        }
    ]
};

export const overTable = {
    tbody: [
        {
            name: 'XXXX作业',
            total: 10,
            startTime: '2020-09-18 12:09',
            overTime: '2020-09-20 11:10',
            score: 3,
            scoreDesc:
                '评价的内容信息在这里进行展示评价的内容信息在这里进行展示评价的内容信息在这里进行展示评价的内容信息在这里进行展示评价的内容信息在这里进行展示'
        },
        {
            name: 'XXXX作业',
            total: 10,
            startTime: '2020-09-18 12:09',
            overTime: '2020-09-20 11:10',
            score: 5
        },
        {
            name: 'XXXX作业',
            total: 10,
            startTime: '2020-09-18 12:09',
            overTime: '2020-09-20 11:10',
            score: 0
        },
        {
            name: 'XXXX作业',
            total: 10,
            startTime: '2020-09-18 12:09',
            overTime: '2020-09-20 11:10'
        }
    ]
};

export const locationOption = [
    {
        label: '一级 1',
        children: [
            {
                label: '二级 1-1',
                children: [
                    {
                        label: '三级 1-1-1'
                    }
                ]
            }
        ]
    },
    {
        label: '一级 2',
        children: [
            {
                label: '二级 2-1',
                children: [
                    {
                        label: '三级 2-1-1'
                    }
                ]
            },
            {
                label: '二级 2-2',
                children: [
                    {
                        label: '三级 2-2-1'
                    }
                ]
            }
        ]
    },
    {
        label: '一级 3',
        children: [
            {
                label: '二级 3-1',
                children: [
                    {
                        label: '三级 3-1-1'
                    }
                ]
            },
            {
                label: '二级 3-2',
                children: [
                    {
                        label: '三级 3-2-1'
                    }
                ]
            }
        ]
    }
];

export const machineOption = [
    {
        label: '一级 1',
        children: [
            {
                label: '二级 1-1',
                children: [
                    {
                        label: '三级 1-1-1'
                    }
                ]
            }
        ]
    },
    {
        label: '一级 2',
        children: [
            {
                label: '二级 2-1',
                children: [
                    {
                        label: '三级 2-1-1'
                    }
                ]
            },
            {
                label: '二级 2-2',
                children: [
                    {
                        label: '三级 2-2-1'
                    }
                ]
            }
        ]
    },
    {
        label: '一级 3',
        children: [
            {
                label: '二级 3-1',
                children: [
                    {
                        label: '三级 3-1-1'
                    }
                ]
            },
            {
                label: '二级 3-2',
                children: [
                    {
                        label: '三级 3-2-1'
                    }
                ]
            }
        ]
    }
];

export const toolOption = [
    {
        label: '一级 1',
        children: [
            {
                label: '二级 1-1',
                children: [
                    {
                        label: '三级 1-1-1'
                    }
                ]
            }
        ]
    },
    {
        label: '一级 2',
        children: [
            {
                label: '二级 2-1',
                children: [
                    {
                        label: '三级 2-1-1'
                    }
                ]
            },
            {
                label: '二级 2-2',
                children: [
                    {
                        label: '三级 2-2-1'
                    }
                ]
            }
        ]
    },
    {
        label: '一级 3',
        children: [
            {
                label: '二级 3-1',
                children: [
                    {
                        label: '三级 3-1-1'
                    }
                ]
            },
            {
                label: '二级 3-2',
                children: [
                    {
                        label: '三级 3-2-1'
                    }
                ]
            }
        ]
    }
];

export const intervalOption = [
    {
        value: '1',
        label: '全天'
    },
    {
        value: '2',
        label: '上午'
    },
    {
        value: '3',
        label: '下午'
    },
    {
        value: '4',
        label: '晚上'
    }
];

export const personOption = [
    {
        id: '1',
        name: '张三'
    },
    {
        id: '2',
        name: '李四'
    },
    {
        id: '3',
        name: '王五'
    },
    {
        id: '4',
        name: '赵六'
    },
    {
        id: '5',
        name: '陈七'
    },
    {
        id: '6',
        name: '孙八'
    }
];

export const jobTypeOption = [
    {
        id: '1',
        name: '定位'
    },
    {
        id: '2',
        name: '收割'
    }
];

export const infoTableData = [
    { name: '张三', phone: '15951475896', process: '待接单' },
    { name: '张三', phone: '15951475896', process: '拒单' },
    { name: '张三', phone: '15951475896', process: '已接单' },
    { name: '张三', phone: '15951475896', process: '进行中' },
    { name: '张三', phone: '15951475896', process: '已完成' }
];

export const assignPersonList = [
    { key: 1, label: '备选项1', disabled: true },
    { key: 2, label: '备选项2' },
    { key: 3, label: '备选项3' },
    { key: 4, label: '备选项4' },
    { key: 5, label: '备选项5' },
    { key: 6, label: '备选项6' },
    { key: 7, label: '备选项7' }
];
