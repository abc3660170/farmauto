import { on, off } from '@utils/dom';
import { jobbillPage } from '@utils/getData';
import { resolveRes } from '@utils/utils';
import log from '@utils/log';

export default {
    data() {
        return {
            jobStatus: '',
            inputValue: '',
            timeValue: [],
            currentPage: 1,
            pageSize: 100,
            tableHeight: 500
        };
    },
    mounted() {
        on(window, 'resize', this.setHeight);
        this.setHeight();
    },
    methods: {
        setHeight() {
            const outerHeight = document.querySelector('.main').clientHeight;
            const tabHeight =
                (document.querySelector('.tabs') && document.querySelector('.tabs').clientHeight) ||
                0;
            const filterHeight =
                (document.querySelector('.filterArea') &&
                    document.querySelector('.filterArea').clientHeight) ||
                0;
            const paginationHeight =
                (document.querySelector('.pagination') &&
                    document.querySelector('.pagination').clientHeight) ||
                0;

            this.tableHeight = outerHeight - tabHeight - filterHeight - paginationHeight;
        },
        pageApi() {
            const params = {
                jobStatus: this.jobStatus,
                jobName: this.inputValue,
                jobType: '',
                startDate: this.timeValue[0] === undefined ? '' : this.timeValue[0],
                endDate: this.timeValue[1] === undefined ? '' : this.timeValue[1],
                page: this.currentPage,
                size: this.pageSize
            };
            log.info(params);
            jobbillPage(params).then(res => {
                resolveRes(res, data => {
                    const tablePage = data.list;
                    tablePage.forEach(item => {
                        item['receive'] = 0;
                        item['reject'] = 0;
                        item['ing'] = 0;
                        item['over'] = 0;
                        const userStatus = item.userStaus;
                        if (userStatus !== undefined && userStatus.length > 0) {
                            userStatus.forEach(item2 => {
                                if (item2.id === '002') {
                                    item.receive = item2.count;
                                } else if (item2.id === '006') {
                                    item.reject = item2.count;
                                } else if (item2.id === '003') {
                                    item.ing = item2.count;
                                } else if (item2.id === '004') {
                                    item.over = item2.count;
                                }
                            });
                        }
                    });
                    this.tableData = tablePage;
                    this.total = data.total;
                });
            });
        },
        reset() {
            this.inputValue = '';
            this.timeValue = [];
            this.currentPage = 1;
            this.pageSize = 100;
            this.pageApi();
        },
        filter() {
            this.pageApi();
            //筛选表格
        },
        handleSizeChange(val) {
            this.pageSize = val;
            this.currentPage = 1;
            this.pageApi();
            //改变表格每页条数
        },
        handleCurrentChange(val) {
            this.currentPage = val;
            this.pageApi();
            //改变分页当前页
        }
    },
    destroy() {
        off(window, 'resize', this.setHeight);
    }
};
