export const workTableData = [
    {
        name: '李四喜',
        massif: '地块信息',
        farmMachine: '农机',
        workTime: '2020-9-19 12:09',
        createTime: '2020-9-19 12:09',
        state: 'progressing',
        score: 2.6
    },
    {
        name: '李四喜',
        massif: '地块信息',
        farmMachine: '农机',
        workTime: '2020-9-19 12:09',
        createTime: '2020-9-19 12:09',
        state: 'completed',
        score: 3.6
    },
    {
        name: '李四喜',
        massif: '地块信息',
        farmMachine: '农机',
        workTime: '2020-9-19 12:09',
        createTime: '2020-9-19 12:09',
        state: 'accepted',
        score: 4.9
    },
    {
        name: '李四喜',
        massif: '地块信息',
        farmMachine: '农机',
        workTime: '2020-9-19 12:09',
        createTime: '2020-9-19 12:09',
        state: 'pending',
        score: null
    },
    {
        name: '李四喜',
        massif: '地块信息',
        farmMachine: '农机',
        workTime: '2020-9-19 12:09',
        createTime: '2020-9-19 12:09',
        state: 'rejection',
        score: null
    },
    {
        name: '李四喜',
        massif: '地块信息',
        farmMachine: '农机',
        workTime: '2020-9-19 12:09',
        createTime: '2020-9-19 12:09',
        state: 'progressing',
        score: null
    },
    {
        name: '李四喜',
        massif: '地块信息',
        farmMachine: '农机',
        workTime: '2020-9-19 12:09',
        createTime: '2020-9-19 12:09',
        state: null,
        score: null
    }
];
