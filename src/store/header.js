export default {
    namespaced: true,
    state: {
        taskList: [],
        taskId: '',
        currentTask: ''
    },
    mutations: {
        updateTaskList(state, taskList) {
            // 用户登陆成功后调用
            state.taskList = taskList;
        },
        updateCurrentTaskId(state, id) {
            state.taskId = id;
        },
        updateCurrentTask(state, task) {
            state.currentTask = task;
        }
    }
};
