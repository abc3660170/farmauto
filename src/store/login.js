import { loginApi } from '@utils/getData';

const login = {
    namespaced: true,
    state: {
        token: null, // 存储在 localstorage 作为已登陆的凭据
        errMsg: '', // 用来显示在登陆页上的错误提示
        userId: '',
        projectId: '',
        realName: '',
        roleName: '',
        loginName: ''
    },
    mutations: {
        success(state, { token, id, projectId, userName, userLoginname, roleName }) {
            sessionStorage.setItem('token', token);
            sessionStorage.setItem('userId', id);
            sessionStorage.setItem('projectId', projectId);
            sessionStorage.setItem('realName', userName);
            sessionStorage.setItem('roleName', roleName);
            sessionStorage.setItem('loginName', userLoginname);
            state.token = token;
            state.userId = id;
            state.projectId = projectId;
            state.realName = userName;
            state.roleName = roleName;
            state.loginName = userLoginname;
            state.errMsg = '';
        },
        fail(state, { errMsg }) {
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('userId');
            sessionStorage.removeItem('projectId');
            sessionStorage.removeItem('realName');
            sessionStorage.removeItem('roleName');
            sessionStorage.removeItem('loginName');
            state.errMsg = errMsg;
        },
        logout(state) {
            // 退出登陆，清理token
            state.user = '';
            state.token = null;
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('userId');
            sessionStorage.removeItem('projectId');
            sessionStorage.removeItem('realName');
            sessionStorage.removeItem('roleName');
            sessionStorage.removeItem('loginName');
        }
    },
    actions: {
        doLogin({ commit }, { username, password }) {
            /**
             * step 1
             * todo 这里添加一些登录成功前的提示 ，
             * 比如页面的瞬间提示组件，
             * 正在 ‘加载中... 请稍后’ 之类。
             */

            /**
             * step 2
             * 登陆请求服务接口（异步的服务）
             * 登陆成功后更新登陆状态（success），失败了也要更新登陆状态（fail）
             * 下面是demo
             */
            return new Promise((resolve, reject) => {
                // todo 关闭页面的登陆中的提示
                const param = {
                    password: password,
                    typecode: 1,
                    userLoginname: username
                };
                loginApi(param).then(response => {
                    if (response.data.code === 0) {
                        // 这里的token字段是后台服务返的
                        const {
                            token,
                            id,
                            projectId,
                            userName,
                            userLoginname,
                            roleName
                        } = response.data.data;
                        commit('success', {
                            token,
                            id,
                            projectId,
                            userName,
                            userLoginname,
                            roleName
                        });
                        resolve();
                    } else {
                        commit('fail', { errMsg: '用户名或密码不正确！' });
                        reject('用户名或密码不正确！');
                    }
                });
            });
        }
    }
};
export default login;
