import Vue from 'vue';
import Vuex from 'vuex';
import login from './login';
import permission from './permission';
import header from './header';

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        login,
        permission,
        header
    }
});

export default store;
