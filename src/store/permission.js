export default {
    namespaced: true,
    state: {
        role: 'admin', //todo为了测试，当前登陆用户的角色
        menu: [],
        leaveNotice: 1, // 头部菜单的请假通知数量, 0 的时候不显示
        taskNotice: 1 // 头部菜单的作业通知数量
    },
    mutations: {
        updateRole(state, backendRole) {
            // 用户登陆成功后调用
            state.role = getRole(backendRole);
        },

        updateMenu(state, menu) {
            // 头部菜单用, 用户登陆成功后调用
            state.menu = menu;
        },

        updateLeaveNotice(state, leaveNotice) {
            state.leaveNotice = leaveNotice;
        },

        updateTaskNotice(state, taskNotice) {
            state.taskNotice = taskNotice;
        }
    }
};

/**
 * 获取当前登陆用户的角色
 * @param {*} backendRole 后台返回的数据，如果和预设的不一样，请修改case值
 */
function getRole(backendRole) {
    let role;
    switch (backendRole) {
        case 'superAdmin': // 超级管理员
            role = 'superAdmin';
            break;
        case 'admin': // 管理员
            role = 'admin';
            break;
        case 'driver': // 驾驶员
            role = 'driver';
            break;
        default:
            throw new Error('登陆成功后未获取到当前登陆用户角色！！！');
    }
    return role;
}
