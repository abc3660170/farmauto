export const testUser = {
    username: 'tchen',
    realname: '陈涛',
    role: '系统管理员',
    lastLogin: '2019-09-02 11:34:11',
    ip: '10.0.4.32'
};

export const taskList = [
    {
        value: '选项1',
        label: 'XXXX作业-1'
    },
    {
        value: '选项2',
        label: 'XXXX作业-2'
    },
    {
        value: '选项3',
        label: 'XXXX作业-3'
    },
    {
        value: '选项4',
        label: 'XXXX作业-4'
    },
    {
        value: '选项5',
        label: 'XXXX作业-5'
    }
];
