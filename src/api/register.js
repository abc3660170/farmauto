import { namespace } from '@config/base';
const nameSpaceForRegister = `${namespace}/register`;
const mocked = true;
export const register = {
    mocked: mocked,
    url: `${nameSpaceForRegister}/register`,
    type: 'post'
};
