import { namespace } from '@config/base';
const nameSpaceForMonitor = `${namespace}/MonitorData`;
const mocked = true;
//分类统计
export const monitorOnOff = {
    mocked: mocked,
    url: `${nameSpaceForMonitor}/MonitorOnOff`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
export const listDbByJob = {
    mocked: mocked,
    url: `${nameSpaceForMonitor}/ListDbByJob`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
