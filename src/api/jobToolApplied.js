import { namespace } from '@config/base';
const nameSpaceForJobbill = `${namespace}/JobToolApplied`;
const mocked = true;
//分类统计
export const queryTool = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/list`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
