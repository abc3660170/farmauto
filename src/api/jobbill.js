import { namespace } from '@config/base';
const nameSpaceForJobbill = `${namespace}/Jobbill`;
const mocked = false;
//分类统计
export const getCount = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/getCount`,
    type: 'get'
};
//分页获取作业列表
export const page = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/page`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//获取作业详情
export const getById = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/getByIDQ`,
    type: 'get'
};
//重新派单中人员
// export const queryUserByRedispatch = {
//     mocked: mocked,
//     url: `${nameSpaceForJobbill}/queryUserByRedispatch`,
//     type: 'post'
// };
//新建作业
export const save = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/saveQ`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//新建中地块
export const queryField = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/queryField`,
    type: 'post'
};
//新建中农机
export const queryMach = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/queryMach`,
    type: 'post'
};
//新建中农具
export const queryTool = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/queryTool`,
    type: 'post'
};
// //新建中的农机手
// export const queryUser = {
//     mocked: mocked,
//     url: `${nameSpaceForJobbill}/queryUser`,
//     type: 'post'
// };
//重新派单
export const redispatch = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/redispatch`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//发布任务
export const update = {
    mocked: false,
    url: `${nameSpaceForJobbill}/setJobState`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//评价
export const setScore = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/setScore`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};

//暂时不用
export const deleteJobbill = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/delete`,
    type: 'get'
};

export const list = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/list`,
    type: 'post'
};
export const listByGet = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/list`,
    type: 'get'
};
export const listAll = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/listall`,
    type: 'post'
};
