import { namespace } from '@config/base';
const nameSpaceForJobbill = `${namespace}/Devicecategory`;
const mocked = true;
//农机分类树
export const getMachDevicecategoryTree = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/getMachDevicecategoryTree`,
    type: 'get'
};
//农具分类树
export const getToolDevicecategoryTree = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/getMachDevicecategoryTree`,
    type: 'get'
};
//农具分类树
export const getMachMergelTree = {
    mocked: false,
    url: `${nameSpaceForJobbill}/getMachmergelTree`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//农具分类树
export const getToolMergelTree = {
    mocked: false,
    url: `${nameSpaceForJobbill}/getToolmergelTree`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
