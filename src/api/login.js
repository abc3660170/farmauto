import { namespace } from '@config/base';
const nameSpaceForLogin = `${namespace}/User`;
const mocked = false;
export const login = {
    mocked: mocked,
    url: `${nameSpaceForLogin}/login`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
