import { namespace } from '@config/base';
const nameSpaceForJobbill = `${namespace}/JobMachApplied`;
const mocked = true;
//分类统计
export const list = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/list`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
