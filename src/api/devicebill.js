import { namespace } from '@config/base';
const nameSpaceForJobbill = `${namespace}/Devicebill`;
const mocked = true;
//农机分页查询
export const pageMach = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/pageMach`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//农具分页查询
export const pageTool = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/pageTool`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//删除机器
export const deleteDevice = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/delete`,
    type: 'get'
};
//新增机器
export const saveDevice = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/save`,
    type: 'get'
};
//修改机器
export const updateDevice = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/update`,
    type: 'get'
};
//获取管理员
export const deviceAdmin = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/deviceAdmin`,
    type: 'get'
};
//获取状态统计值
export const machCount = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/machCount`,
    type: 'get'
};
