import { namespace } from '@config/base';
const nameSpaceForJobbill = `${namespace}/JobUserApplied`;
const mocked = false;
//分类统计
export const queryJobUser = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/list`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
