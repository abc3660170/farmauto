import { namespace } from '@config/base';
const nameSpaceForGrid = `${namespace}/Grid`;
const mocked = false;
export const deleteGrid = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/delete`,
    type: 'get'
};
export const deleteById = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/deletebyidQ`,
    type: 'get'
};
export const getById = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/getByIDQ`,
    type: 'get'
};
export const getGridByFullName = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/getGridbyFullname`,
    type: 'get'
};
export const getGridFullNameById = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/getGridFullnamebyId`,
    type: 'get'
};
export const getGridIdByFullName = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/getGridIdbyFullname`,
    type: 'get'
};
export const getGridPathById = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/getGridPathbyId`,
    type: 'get'
};
export const getGridTreeByPlantId = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/getGridTreebyPlantIdQ`,
    type: 'get'
};
export const list = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/list`,
    type: 'get'
};
export const listAll = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/listall`,
    type: 'get'
};
export const page = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/page`,
    type: 'get'
};
export const save = {
    mocked: mocked,
    url: `${nameSpaceForGrid}/save`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
