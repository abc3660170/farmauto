import { namespace } from '@config/base';
const nameSpaceForDictionary = `${namespace}/DataDictionary`;
const mocked = false;
export const getDataDictionarys = {
    mocked: mocked,
    url: `${nameSpaceForDictionary}/getDataDictionarys`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
