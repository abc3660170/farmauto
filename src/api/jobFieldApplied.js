import { namespace } from '@config/base';
const nameSpaceForJobbill = `${namespace}/JobFieldApplied`;
const mocked = true;
//分类统计
export const queryField = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/list`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
