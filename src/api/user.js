import { namespace } from '@config/base';
const nameSpaceForJobbill = `${namespace}/User`;
const mocked = true;
//工作状态下的农机手
export const queryUser = {
    mocked: false,
    url: `${nameSpaceForJobbill}/list`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//分页农机手
export const pageUser = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/page`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//农机手状态统计
export const userStatusCount = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/userStatusCount`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//重置农机手密码
export const resetPass = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/resetPass`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//删除农机手
export const delPersonnel = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/delete`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//解聘农机手
export const dismissEmploy = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/dismissEmploy`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//聘用农机手
export const employ = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/employ`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//聘用农机手
export const saveDriver = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/saveDriver`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
//聘用农机手
export const updateDriver = {
    mocked: mocked,
    url: `${nameSpaceForJobbill}/updateDriver`,
    type: 'post',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
};
