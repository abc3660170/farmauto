import Farmauto from './Farmauto';
import Vue from 'vue';
import router from './router';
import store from './store';
import ElementUI from 'element-ui';
import axios from 'axios';
import { apiBaseUrl as baseUrl } from '@config/base.js';
import './assets/stylesheet/normalize.scss';
import 'element-ui/lib/theme-chalk/index.css';
import 'video.js/dist/video-js.css';
// import 'vue-video-player/src/custom-theme.css'
const hls = require('videojs-contrib-hls');
Vue.use(hls);
let MockP;
if (process.env.NODE_ENV === 'development') {
    MockP = import('./mock/index');
}
axios.defaults.baseURL = baseUrl;
axios.defaults.headers = {
    'Content-Type': 'application/json;charset=UTF-8'
};

Vue.use(ElementUI);
Vue.prototype.$axios = axios;

import AmapVue from '@amap/amap-vue';

AmapVue.config.key = '0b9182e539cb986ccca5afe9bc81df29';
AmapVue.config.plugins = [
    'AMap.ToolBar',
    'AMap.MoveAnimation'
    // 在此配置你需要预加载的插件，如果不配置，在使用到的时候会自动异步加载
];
Vue.use(AmapVue);

(MockP || Promise.resolve()).then(() => {
    new Vue({
        el: '#app',
        router,
        store,
        components: { Farmauto },
        template: '<Farmauto />'
    });
});
