import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);

import login from '@pages/login';
import driver from '@pages/driver/driver';
import guidance from '@pages/guidance/guidance';
import taskManage from '@pages/taskManage/TaskManage';
import leaveInfo from '@pages/leaveInfo/leaveInfo';
import farmMachine from '@pages/farmMachine/farmMachine';
import farmlandManage from '@pages/farmlandManage/FarmlandManage';
import intimeData from '@pages/intimeData/IntimeData';
import historyData from '@pages/historyData/HistoryData';

import farmTools from '@pages/farmTools/farmTools';
import terminal from '@pages/terminal/terminal';

import workList from '@pages/workList/workList';
import leaveApply from '@pages/leaveApply/leaveApply';
import employHistory from '@pages/employHistory/employHistory';

import projectManage from '@pages/projectManage/projectManage';
import adminManage from '@pages/adminManage/adminManage';
import statistics from '@pages/statistics/statistics';

import notFound from '@pages/404';
import log from '@utils/log';

// 和交互确认过，路由比较简单可以在页面中按照用户角色写死。

const basicRoutes = [
    {
        path: '/login',
        name: 'login',
        component: login
    },
    {
        path: '*',
        component: notFound
    }
];
// 超级管理员路由
const superAdminRoutes = [
    {
        path: '/projectManage',
        name: 'projectManage',
        component: projectManage,
        meta: {
            requireAuth: true,
            chineseName: '项目管理',
            englishName: 'projectManage'
        }
    },
    {
        path: '/adminManage',
        name: 'adminManage',
        component: adminManage,
        meta: {
            requireAuth: true,
            chineseName: '系统管理员管理',
            englishName: 'adminManage'
        }
    },
    {
        path: '/statistics',
        name: 'statistics',
        component: statistics,
        meta: {
            requireAuth: true,
            chineseName: '项目统计',
            englishName: 'statistics'
        }
    }
];

// 管理员路由
export const adminRoutes = [
    {
        path: '/driver',
        name: 'driver',
        component: driver,
        meta: {
            requireAuth: true,
            chineseName: '农机手管理',
            englishName: 'driver'
        }
    },
    {
        path: '/leaveInfo',
        name: 'leaveInfo',
        component: leaveInfo,
        meta: {
            requireAuth: true,
            chineseName: '请假信息',
            englishName: 'leaveInfo'
        }
    },
    {
        path: '/farmMachine',
        name: 'farmMachine',
        component: farmMachine,
        meta: {
            requireAuth: true,
            chineseName: '农机管理',
            englishName: 'farmMachine'
        }
    },
    {
        path: '/farmTools',
        name: 'farmTools',
        component: farmTools,
        meta: {
            requireAuth: true,
            chineseName: '农具管理',
            englishName: 'farmTools'
        }
    },
    {
        path: '/terminal',
        name: 'terminal',
        component: terminal,
        meta: {
            requireAuth: true,
            chineseName: '监控终端管理',
            englishName: 'terminal'
        }
    },
    {
        path: '/guidance',
        name: 'guidance',
        component: guidance,
        meta: {
            requireAuth: true,
            // TODO  不在菜单中出现 chineseName: '引导页',
            englishName: 'guidance'
        }
    },
    {
        path: '/taskManage',
        name: 'taskManage',
        component: taskManage,
        meta: {
            requireAuth: true,
            chineseName: '作业管理',
            englishName: 'taskManage'
        }
    },
    {
        path: '/farmlandManage',
        name: 'farmlandManage',
        component: farmlandManage,
        meta: {
            requireAuth: true,
            chineseName: '地块管理',
            englishName: 'farmlandManage'
        }
    },
    {
        path: '/statistics',
        name: 'statistics',
        component: statistics,
        meta: {
            requireAuth: true,
            chineseName: '项目统计',
            englishName: 'statistics'
        }
    },
    {
        path: '/intimeData',
        name: 'intimeData',
        component: intimeData,
        meta: {
            requireAuth: true,
            chineseName: '实时数据',
            englishName: 'intimeData'
        }
    },
    {
        path: '/historyData',
        name: 'historyData',
        component: historyData,
        meta: {
            requireAuth: true,
            chineseName: '历史数据',
            englishName: 'historyData'
        }
    }

    //todo 這裡配置你這個項目其他页面的路由
];

// 农机手路由
const driverRoutes = [
    {
        path: '/workList',
        name: 'workList',
        component: workList,
        meta: {
            requireAuth: true,
            chineseName: '作业列表',
            englishName: 'workList'
        }
    },
    {
        path: '/leaveApply',
        name: 'leaveApply',
        component: leaveApply,
        meta: {
            requireAuth: true,
            chineseName: '请假申请',
            englishName: 'leaveApply'
        }
    },
    {
        path: '/employHistory',
        name: 'employHistory',
        component: employHistory,
        meta: {
            requireAuth: true,
            chineseName: '聘用历史',
            englishName: 'employHistory'
        }
    }
];

const router = new Router({
    routes: [...basicRoutes, ...adminRoutes, ...superAdminRoutes, ...driverRoutes]
});

export default router;

/*************************** 前置路由守卫 *************************************
 * 可参考 http://10.0.225.88:30080/fee/ShyBee/blob/develop/src/router/index.js
 *****************************************************************************/
router.beforeEach((to, from, next) => {
    const role = router.app.$options.store.state.permission.role;
    const routes = getRoutesByRole(role);
    /**
     * 请开发者删除下面的next()方法
     * 直接调用next完成路由是为了演示
     */
    //next();

    // 未登录前访问需要权限的页面直接跳转到登陆页

    handleRouteBeforeLogin(to, next);

    // 处理默认路由
    if (isRouteToDefault(to)) {
        handleDefaultRoute(routes, next);
    }

    // 简单用户权限判断
    handleRouteAuth(routes, to, next);
});

/**
 * 未登录前访问需要权限的页面直接跳转到登陆页
 * @param {*} route
 * @param {*} next
 */
function handleRouteBeforeLogin(to, next) {
    if (to.matched.some(r => r.meta.requireAuth) && sessionStorage.getItem('token') === null) {
        next({ path: '/login' }).catch(e => {
            log.error(e);
        });
    }
}

/**
 * 访问了网站根目录
 * @param {*} route
 */
function isRouteToDefault(route) {
    return route.matched.length === 1 && route.path === '/';
}

/**
 * 路由到有权限的第一个模块
 * @param {*} next
 */
function handleDefaultRoute(routes, next) {
    next({ path: routes[0].path });
}

/**
 * 通过对应角色的路由表的name去判断是否有访问权限，防止盗链
 * @param {*} routes
 * @param {*} route
 * @param {*} next
 */
function handleRouteAuth(routes, route, next) {
    const allowRoutesName = routes.map(_route => _route.name);
    if (allowRoutesName.indexOf(route.name) > -1 || route.matched.every(r => !r.meta.requireAuth)) {
        next();
    } else {
        next({ path: '/login' });
    }
}

export function getRoutesByRole(role) {
    if (role === 'superAdmin') {
        return superAdminRoutes;
    } else if (role === 'admin') {
        return adminRoutes;
    } else if (role === 'driver') {
        return driverRoutes;
    }
}
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location, onResolve, onReject) {
    if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject);
    return originalPush.call(this, location).catch(err => err);
};
