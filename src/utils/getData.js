import fetch from './fetch';
import { login } from '@api/login';
import { register } from '@api/register';

export const loginApi = data => fetch(login, data);
export const registerApi = data => fetch(register, data);
// 地块管理 api
import {
    getGridTreeByPlantId as _getGridTreeByPlantId,
    getById as _getGridById,
    save as _saveGrid,
    deleteById as _deleteById
} from '@api/grid';
export const getGridTreeByPlantId = data => fetch(_getGridTreeByPlantId, data);
export const getGridById = data => fetch(_getGridById, data);
export const saveGrid = data => fetch(_saveGrid, data);
export const deleteGridById = data => fetch(_deleteById, data);

//字典表
import { getDataDictionarys as _getDataDictionarys } from '@api/dictionary';
export const getDataDictionarys = data => fetch(_getDataDictionarys, data);

//作业管理
import {
    getCount as _jobbillGetCount,
    page as _jobbillPage,
    getById as _jobbillGetById,
    save as _jobbillSave,
    queryField as _jobbillQueryField,
    redispatch as _jobbillRedispatch,
    update as _jobbillUpdate,
    setScore as _jobbillSetScore,
    list as _jobbillList
} from '@api/jobbill';
export const jobbillGetCount = data => fetch(_jobbillGetCount, data);
export const jobbillPage = data => fetch(_jobbillPage, data);
export const jobbillGetById = data => fetch(_jobbillGetById, data);
export const jobbillSave = data => fetch(_jobbillSave, data);
export const jobbillQueryField = data => fetch(_jobbillQueryField, data);
export const jobbillRedispatch = data => fetch(_jobbillRedispatch, data);
export const jobbillUpdate = data => fetch(_jobbillUpdate, data);
export const jobbillSetScore = data => fetch(_jobbillSetScore, data);
export const jobbillList = data => fetch(_jobbillList, data);
//作业农机手关联表
import { queryJobUser as _queryJobUser } from '@api/jobUserApplied';
export const queryJobUserApi = data => fetch(_queryJobUser, data);
//作业农机关联表
import { list as _listJobMachApplied } from '@api/jobMachApplied';
export const listJobMachApplied = data => fetch(_listJobMachApplied, data);
//作业农具关联表
import { queryTool as _queryTool } from '@api/jobToolApplied';
export const queryToolApi = data => fetch(_queryTool, data);
//作业地块关联表
import { queryField as _queryField } from '@api/jobFieldApplied';
export const queryFieldApi = data => fetch(_queryField, data);

//机器管理
import {
    pageMach as _pageMach,
    deleteDevice as _deleteDevice,
    saveDevice as _saveDevice,
    updateDevice as _updateDevice,
    deviceAdmin as _deviceAdmin,
    machCount as _machCount
} from '@api/devicebill';
export const pageMachApi = data => fetch(_pageMach, data);
export const deleteDeviceApi = data => fetch(_deleteDevice, data);
export const saveDeviceApi = data => fetch(_saveDevice, data);
export const updateDeviceApi = data => fetch(_updateDevice, data);
export const deviceAdminApi = data => fetch(_deviceAdmin, data);
export const machCountApi = data => fetch(_machCount, data);

//机器分类管理
import {
    getMachDevicecategoryTree as _getMachDevicecategoryTree,
    getMachMergelTree as _getMachMergelTree,
    getToolMergelTree as _getToolMergelTree
} from '@api/devicecategory';
export const machTreeApi = data => fetch(_getMachDevicecategoryTree, data);
export const jobbillQueryMach = data => fetch(_getMachMergelTree, data);
export const jobbillQueryTool = data => fetch(_getToolMergelTree, data);

//农机手信息
import {
    queryUser as _queryUsers,
    pageUser as _pageUser,
    userStatusCount as _userStatusCount,
    resetPass as _resetPass,
    delPersonnel as _delPersonnel,
    dismissEmploy as _dismissEmploy,
    employ as _employ,
    saveDriver as _saveDriver,
    updateDriver as _updateDriver
} from '@api/user';
export const jobbillQueryUser = data => fetch(_queryUsers, data);
export const pageUserApi = data => fetch(_pageUser, data);
export const userStatusCountApi = data => fetch(_userStatusCount, data);
export const resetPassApi = data => fetch(_resetPass, data);
export const delPersonnelApi = data => fetch(_delPersonnel, data);
export const dismissEmployApi = data => fetch(_dismissEmploy, data);
export const employApi = data => fetch(_employ, data);
export const saveDriverApi = data => fetch(_saveDriver, data);
export const updateDriverApi = data => fetch(_updateDriver, data);
//监控数据对象
import { monitorOnOff as _monitorOnOff, listDbByJob as _listDbByJob } from '@api/monitorData';
export const monitorOnOff = data => fetch(_monitorOnOff, data);
export const listDbByJob = data => fetch(_listDbByJob, data);
