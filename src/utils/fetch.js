/*eslint no-multiple-empty-lines:0*/
import { apiBaseUrl } from '@config/base';
import axios from 'axios';
import qs from 'qs';
import log from '@utils/log';
//import th from 'element-ui/src/locale/lang/th';
//import { config } from '@amap/amap-vue';
//import el from 'element-ui/src/locale/lang/el';
import ElementUI from 'element-ui';
const $axios = axios.create({
    baseURL: apiBaseUrl
});

$axios.defaults.withCredentials = true;
$axios.defaults.headers.get['Content-Type'] = 'application/x-www-form-urlencoded';
$axios.defaults.headers.put['Content-Type'] = 'application/json;charset=UTF-8';
$axios.defaults.transformRequest = [
    function(data, headers) {
        // Do whatever you want to transform the data
        if (headers['Content-Type'] === 'application/json;charset=UTF-8') {
            return JSON.stringify(data);
        } else if (headers['Content-Type'] === 'multipart/form-data') {
            return data;
        }
        log.info(qs.stringify(data, { allowDots: true }));
        return qs.stringify(data, { allowDots: true });
    }
];
$axios.interceptors.request.use(
    config => {
        const userId = sessionStorage.getItem('userId');
        const projectId = sessionStorage.getItem('projectId');
        const token = sessionStorage.getItem('token');
        if (token) {
            config.headers['Authorization'] = token;
        }
        if (userId) {
            config.headers['User-ID'] = userId;
        }
        if (projectId) {
            config.headers['Project-ID'] = projectId;
        }
        if (config.method === 'get' && Object.keys(config.data).length > 0) {
            config.url = `${config.url}?${qs.stringify(config.data, { allowDots: true })}`;
        }
        return config;
    },
    err => {
        return Promise.reject(err);
    }
);
$axios.interceptors.response.use(
    config => {
        log.info(config.data);
        if (config.data.code !== 0) {
            ElementUI.Message.error(config.data.msg);
        }
        return config;
    },
    err => {
        log.error(err);
    }
);

export default (config = {}, data = {}) => {
    const { type = 'get', url, headers } = config;
    const method = type.toLowerCase();
    const defaultHeaders = $axios.defaults.headers;
    return $axios({ method, url, data, headers: headers || defaultHeaders });
};
