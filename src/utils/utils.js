export const resolveRes = function(res, successCallBack, failCallBack) {
    if (res && res.status === 200 && res.data.code === 0) {
        successCallBack && successCallBack(res.data.data);
    } else {
        failCallBack && failCallBack(res.data);
    }
};
export const getCenterPos = function(posArr, AMap) {
    const total = posArr.length;
    let X = 0;
    let Y = 0;
    let Z = 0;
    posArr.forEach(pos => {
        //const lnglat = new AMap.LngLat(pos[0], pos[1]);
        const lng = (pos[0] * Math.PI) / 180;
        const lat = (pos[1] * Math.PI) / 180;
        const x = Math.cos(lat) * Math.cos(lng);
        const y = Math.cos(lat) * Math.sin(lng);
        const z = Math.sin(lat);
        X += x;
        Y += y;
        Z += z;
    });

    X /= total;
    Y /= total;
    Z /= total;

    const Lng = Math.atan2(Y, X);
    const Hyp = Math.sqrt(X * X + Y * Y);
    const Lat = Math.atan2(Z, Hyp);

    return new AMap.LngLat((Lng * 180) / Math.PI, (Lat * 180) / Math.PI);
};
