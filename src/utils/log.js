/* eslint-disable */
const merge = function() {
    var args = arguments,
        target = args[0],
        key,
        i;
    for (i = 1; i < args.length; i++) {
        for (key in args[i]) {
            if (!(key in target) && args[i].hasOwnProperty(key)) {
                target[key] = args[i][key];
            }
        }
    }
    return target;
};

const defineLogLevel = function(value, name) {
    return { value: value, name: name };
};

const createDefaultHandler = function(options) {
    options = options || {};

    options.formatter =
        options.formatter ||
        function defaultMessageFormatter(messages, context) {
            // Prepend the logger's name to the log message for easy identification.
            if (context.name) {
                messages.unshift('[' + context.name + ']');
            }
        };

    // Map of timestamps by timer labels used to track `#time` and `#timeEnd()` invocations in environments
    // that don't offer a native console method.
    var timerStartTimeByLabelMap = {};

    // Support for IE8+ (and other, slightly more sane environments)
    var invokeConsoleMethod = function(hdlr, messages) {
        Function.prototype.apply.call(hdlr, console, messages);
    };

    // Check for the presence of a logger.
    if (typeof console === 'undefined') {
        return function() {
            /* no console */
        };
    }

    return function(messages, context) {
        // Convert arguments object to Array.
        messages = Array.prototype.slice.call(messages);

        var hdlr = console.log;
        var timerLabel;

        if (context.level === Logger.TIME) {
            timerLabel = (context.name ? '[' + context.name + '] ' : '') + messages[0];

            if (messages[1] === 'start') {
                if (console.time) {
                    console.time(timerLabel);
                } else {
                    timerStartTimeByLabelMap[timerLabel] = new Date().getTime();
                }
            } else if (console.timeEnd) {
                console.timeEnd(timerLabel);
            } else {
                invokeConsoleMethod(hdlr, [
                    timerLabel +
                        ': ' +
                        (new Date().getTime() - timerStartTimeByLabelMap[timerLabel]) +
                        'ms'
                ]);
            }
        } else {
            // Delegate through to custom warn/error loggers if present on the console.
            if (context.level === Logger.WARN && console.warn) {
                hdlr = console.warn;
            } else if (context.level === Logger.ERROR && console.error) {
                hdlr = console.error;
            } else if (context.level === Logger.INFO && console.info) {
                hdlr = console.info;
            } else if (context.level === Logger.DEBUG && console.debug) {
                hdlr = console.debug;
            } else if (context.level === Logger.TRACE && console.trace) {
                hdlr = console.trace;
            }

            options.formatter(messages, context);
            invokeConsoleMethod(hdlr, messages);
        }
    };
};

// const defaultLogHandler = function(messages, { level }) {
//     messages.unshift(`[${level.name}]`);
// };

const Logger = {};
// Predefined logging levels.
Logger.TRACE = defineLogLevel(1, 'TRACE');
Logger.DEBUG = defineLogLevel(2, 'DEBUG');
Logger.INFO = defineLogLevel(3, 'INFO');
Logger.TIME = defineLogLevel(4, 'TIME');
Logger.WARN = defineLogLevel(5, 'WARN');
Logger.ERROR = defineLogLevel(8, 'ERROR');
Logger.OFF = defineLogLevel(99, 'OFF');

class ContextualLogger {
    constructor(defaultContext = { filterLevel: Logger.OFF }) {
        this.context = defaultContext;
        this.setLevel(defaultContext.filterLevel);
        this.log = this.info;
        this.setHandler(createDefaultHandler(defaultContext));
    }

    setLevel(newLevel) {
        // Ensure the supplied Level object looks valid.
        if (newLevel && 'value' in newLevel) {
            this.context.filterLevel = newLevel;
        }
    }

    // Gets the current logging level for the logging instance
    getLevel() {
        return this.context.filterLevel;
    }

    // Is the logger configured to output messages at the supplied level?
    enabledFor(lvl) {
        var filterLevel = this.context.filterLevel;
        return lvl.value >= filterLevel.value;
    }

    trace() {
        this.invoke(Logger.TRACE, arguments);
    }

    debug() {
        this.invoke(Logger.DEBUG, arguments);
    }

    info() {
        this.invoke(Logger.INFO, arguments);
    }

    warn() {
        this.invoke(Logger.WARN, arguments);
    }

    error() {
        this.invoke(Logger.ERROR, arguments);
    }

    time(label) {
        if (typeof label === 'string' && label.length > 0) {
            this.invoke(Logger.TIME, [label, 'start']);
        }
    }

    timeEnd(label) {
        if (typeof label === 'string' && label.length > 0) {
            this.invoke(Logger.TIME, [label, 'end']);
        }
    }

    // Invokes the logger callback if it's not being filtered.
    invoke(level, msgArgs) {
        if (this.logHandler && this.enabledFor(level)) {
            this.logHandler(msgArgs, merge({ level: level }, this.context));
        }
    }

    setHandler(func) {
        this.logHandler = func;
    }
}

const logLevel = process.env.NODE_ENV === 'development' ? Logger.DEBUG : Logger.WARN;
export default new ContextualLogger({
    filterLevel: logLevel,
    formatter: function(messages, { level }) {
        messages.unshift(`[${level.name}]`);
    }
});
