export const validatorLable = (rule, value, callback) => {
    if (!value) {
        return callback(new Error(rule.message));
    }
    if (value.lable.length > 0) {
        callback();
    } else {
        return callback(new Error(rule.message));
    }
};
