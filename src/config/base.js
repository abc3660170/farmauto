const hostMatch = /^(?:\d+[.]){3}\d+[:]\d+$/;
let host = 'www.ujsiot.top:9988';
//let host = '192.168.1.104:9988';
if (hostMatch.test(process.env.HOST)) {
    host = process.env.HOST;
}

export const apiBaseUrl = 'https://' + host;
export const namespace = '/API';
export { host };
