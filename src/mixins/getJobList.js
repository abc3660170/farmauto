import { jobbillList } from '@utils/getData';
import { resolveRes } from '@utils/utils';

export default {
    methods: {
        getJobList() {
            return new Promise((resolve, reject) => {
                jobbillList(this.jobQueryParam)
                    .then(res => {
                        resolveRes(res, data => {
                            const taskList = [];
                            data.list.forEach(d => {
                                taskList.push({
                                    value: d.id,
                                    label: d.jobName
                                });
                            });
                            this.$store.commit('header/updateTaskList', taskList);
                            this.$nextTick(() => {
                                this.$store.commit('header/updateCurrentTaskId', this.jobId);
                                resolve();
                            });
                        });
                    })
                    .catch(() => {
                        reject();
                    });
            });
        }
    },
    computed: {
        currentTaskId() {
            return this.$store.state.header.taskId;
        },
        taskList() {
            return this.$store.state.header.taskList;
        }
    }
};
