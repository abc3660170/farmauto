export default {
    computed: {
        userId() {
            return this.$store.state.login.userId;
        }
    }
};
