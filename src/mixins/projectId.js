export default {
    computed: {
        projectId() {
            return this.$store.state.login.projectId;
        }
    }
};
